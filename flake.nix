{
  description = "A very basic flake";

  nixConfig = {
    substituters = [
      "https://prismlauncher.cachix.org"
      "https://nix-community.cachix.org"
      "https://hyprland.cachix.org"
      "https://cache.nixos.org"
    ];

    trusted-public-keys = [
      "prismlauncher.cachix.org-1:GhJfjdP1RFKtFSH3gXTIQCvZwsb2cioisOf91y/bK0w="
      "cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY="
      "hyprland.cachix.org-1:a7pgxzMz7+chwVL3/pzj6jIBMioiJM7ypFP8PwtkuGc="
      "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
    ];
  };

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.05";

    home-manager = {
      url = "github:nix-community/home-manager/release-23.05";
    };

    impermanence.url = "github:nix-community/impermanence";

    flake-utils.url = "github:numtide/flake-utils";

    i3switcher = {
      url = "github:grenewode/i3switcher";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };

    nixos-hardware.url = "github:NixOS/nixos-hardware/master";

    emacs-overlay = {
      url = "github:nix-community/emacs-overlay";
    };

    nixos-generators = {
      url = "github:nix-community/nixos-generators";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nur.url = "github:nix-community/NUR";

    prismlauncher.url = "github:PrismLauncher/PrismLauncher/6.3";

    neovim = { url = "github:neovim/neovim/v0.8.3?dir=contrib"; };

    hyprland.url = "github:hyprwm/Hyprland";
  };

  outputs = { self, nixpkgs, impermanence, home-manager
    , flake-utils, i3switcher, emacs-overlay, nur, prismlauncher, neovim
    , hyprland, ... }@inputs:
    with nixpkgs.lib;
    let
      overlays = [ (import emacs-overlay) self.overlay prismlauncher.overlay ];

      nixosModules = importAll ./modules;
      homeManagerModules = importAll ./home-manager-modules;

      mkHost = configuration:
        (nixosSystem {
          specialArgs = inputs // { inherit self; };

          modules = (attrValues self.nixosModules) ++ [
            configuration
            nur.nixosModules.nur
            impermanence.nixosModules.impermanence
            home-manager.nixosModules.home-manager
            {
              nixpkgs = { inherit overlays; };

              home-manager.sharedModules = (attrValues self.homeManagerModules)
                ++ [ impermanence.nixosModules.home-manager.impermanence ];

            }
          ];
        });

      hosts = [ (mkHost ./hosts/mercury) (mkHost ./hosts/terra) ];
      systems = unique (map ({ config, ... }: config.nixpkgs.system) hosts);

      importAll = path:
        with nixpkgs.lib.filesystem;
        let
          paths =
            if pathIsDirectory path then listFilesRecursive path else [ path ];

          nixFiles = filter (nixpkgs.lib.strings.hasSuffix ".nix") paths;
          removeRoot = nixpkgs.lib.removePrefix "${(toString path)}/";
          removeDefault = nixpkgs.lib.removeSuffix "/default.nix";
          removeNix = nixpkgs.lib.removeSuffix ".nix";
          normalizePath = item:
            nixpkgs.lib.pipe item [
              toString
              removeDefault
              removeNix
              removeRoot
            ];

        in listToAttrs (map (nixFile: {
          name = normalizePath nixFile;
          value = import nixFile;
        }) nixFiles);

    in {
      overlay = final: prev: {
        grenewode = {
          blender = final.callPackage ./packages/blender { };
          proton-bridge-cli =
            final.callPackage ./packages/proton-bridge-cli { };

          inherit (i3switcher.packages.${final.system}) i3switcher;
        };

        unstable = nixpkgs-unstable.legacyPackages.${prev.system};

        obs-studio-plugins = prev.obs-studio-plugins // {
          obs-ptz = final.qt6Packages.callPackage ./packages/obs-ptz.nix { };
        };

        installer = import ./lib/install-iso.nix { inherit self system; };
      };

      inherit nixosModules homeManagerModules;

      nixosPresets = (mapAttrs (module: _:
        (import ./lib/preset) module (import "${./presets}/${module}"))
        (builtins.readDir ./presets));

      nixosConfigurations = listToAttrs (map (config: {
        name = config.config.networking.hostName;
        value = config;
      }) hosts);

      nixosVirtualMachines =
        builtins.mapAttrs (name: { config, ... }: config.system.build.vm)
        self.nixosConfigurations;
    };
}
