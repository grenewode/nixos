{ python3, xorg, stdenv, gtk3, gdk-pixbuf, pygobject3, fetchFromGitHub, lib
, writeScript }:
stdenv.mkDerivation rec {
  pname = "oomox";
  version = "1.14";

  src = fetchFromGitHub {
    owner = "themix-project";
    repo = pname;
    rev = version;
    hash = "sha256-MeLfVmc+9KjuDx8uJZ9t2HF81a+SWJSueFNZWRVE4YA=";
  };

  buildInputs = [ ];
  nativeBuildInputs = [ gtk3 gdk-pixbuf xorg.xrdb pygobject3 ];

  installPhase = let oomox-gui = writeScript "oomox-gui" "";
  in ''
    install -Dp -m 755 "$src/oomox_gui"
  '';
}
