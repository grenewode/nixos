{ buildGoModule, pkg-config, libsecret, which, lib, fetchFromGitHub }:
buildGoModule rec {
  pname = "proton-bridge-cli";
  version = "1.8.12";

  nativeBuildInputs = [ pkg-config which ];
  buildInputs = [ libsecret ];

  src = fetchFromGitHub {
    owner = "ProtonMail";
    repo = "proton-bridge";

    rev = "v${version}";

    hash = "sha256-CkvD7PKx2Gm2KgsIIqQ9l1Z3tWlhqIbW0sxCV2UBYTE=";
  };

  vendorSha256 = "sha256-Pz3xRTwlnJGh1XvxIbyjvYcMywk/wdp4hYovPLBD494=";

  buildPhase = ''
    mkdir -p "$out/bin"

    patchShebangs ./utils/credits.sh
    substituteInPlace Makefile \
      --replace "\$(shell git rev-parse --short=10 HEAD)" "${src.rev}" \
    # patchShebangs Makefile 

    make build-nogui

    ln -s ./proton-bridge "$out/bin/proton-bridge-cli" 
  '';

  doCheck = false;
}
