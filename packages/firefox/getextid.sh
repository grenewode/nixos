#!/usr/bin/env sh

EXT_TMP=$(mktemp)

cleanup() {
	rm -f "$EXT_TMP"
}

trap cleanup EXIT

curl -L "$1" >"$EXT_TMP" || exit $!
{
	unzip -p "$EXT_TMP" "manifest.json" || exit $!
} | jq ".browser_specific_settings.gecko.id"

# jq ".applications.gecko.id"
