{ lib, stdenv, fetchFromGitHub, cmake, obs-studio, qtbase, qtsvg, wrapQtAppsHook
, git }:
stdenv.mkDerivation rec {

  pname = "obs-ptz";
  version = "0.13.1";

  src = fetchFromGitHub {
    owner = "glikely";
    repo = pname;
    rev = "v${version}";
    hash = "sha256-O+87+1C81lcEFDXmGS6NiikxxDE1NPb74CkRPNMfO54=";
  };

  preConfigure = ''
    substituteInPlace ./CMakeLists.txt --replace "git describe --tags --dirty" "echo v${version}"
  '';

  installPhase = ''
    runHook preInstall

    mkdir -p $out/lib/obs-plugins/
    cp *.so $out/lib/obs-plugins/

    mkdir -p $out/share/obs/obs-plugins/${pname}
    cp -r ./rundir/Release/data/obs-plugins/obs-ptz/data/. $out/share/obs/obs-plugins/${pname}/

    runHook postInstall
  '';

  nativeBuildInputs = [ git cmake wrapQtAppsHook ];
  buildInputs = [ obs-studio qtbase qtsvg ];
}
