{ lib, fetchFromGitHub, fetchurl, system }:
let
  pkgs = import (fetchFromGitHub {
    owner = "NixOS";
    repo = "nixpkgs";

    rev = "0a60663e67fd32a27301aecad3a3bbca76932c39";
    hash = "sha256-BF3oqbhWbaDT3nhf3nu3AZFgqqzl0ldgmMkZyeJvxz4=";
  }) { inherit system; };
in pkgs.blender.overrideAttrs (attrs: rec {
  pname = "blender";
  version = "3.4.1";

  src = fetchurl {
    url = "https://download.blender.org/source/${pname}-${version}.tar.xz";
    hash = "sha256-JHxMEignDJAQ9HIcmFy1tiirUKvPnyZ4Ywc3FC7rkcM=";
  };

  buildInputs = attrs.buildInputs ++ (with pkgs; [
    pkg-config
    wayland
    libxkbcommon
    dbus
    libffi
    wayland-protocols
    epoxy
    brotli
    libdecor
  ]);

  cmakeFlags = attrs.cmakeFlags ++ [
    "-DWITH_GHOST_WAYLAND=ON"
    "-DWITH_SYSTEM_GLES=ON"
    "-DWITH_GL_EGL=ON"
    "-DWITH_MOD_FLUID=ON"
    "-DWITH_MOD_OCEANSIM=ON"
    "-DWITH_MOD_REMESH=ON"
    "-DWITH_BUILDINFO=ON"
  ];
})
