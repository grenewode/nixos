{ config, lib, options, ... }:
with lib; {
  programs.ssh = {
    enable = mkDefault true;
    userKnownHostsFile = "${config.home.homeDirectory}/.ssh/known_hosts";
  };

  home.state = mkIf (options.home ? state) {
    files =
      (with config.programs; lists.optional ssh.enable ssh.userKnownHostsFile);
  };
}
