{ pkgs, config, lib, ... }: {
  programs.tmux = {
    enable = true;

    baseIndex = 1;
    clock24 = true;

    keyMode = "vi";

    newSession = true;

    secureSocket = true;

    sensibleOnTop = true;

    extraConfig = ''
      set -g mouse on
    '';

    plugins = with pkgs; [

      tmuxPlugins.cpu
      tmuxPlugins.net-speed

      {
        plugin = tmuxPlugins.yank;
        extraConfig = ''
          set -g @custom_copy_command '${xsel}/bin/xsel'
        '';
      }

      {
        plugin = tmuxPlugins.resurrect;
        extraConfig = "set -g @resurrect-strategy-nvim 'session'";
      }

      {
        plugin = tmuxPlugins.continuum;
        extraConfig = ''
          set -g @continuum-restore 'on'
          set -g @continuum-save-interval '60' # minutes
        '';
      }

    ];
  };
}
