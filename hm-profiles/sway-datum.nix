{ config, lib, pkgs, nixosConfig, self, ... }:
with lib;
let
  wellknown = import "${self}/lib/wellknown.nix" { inherit lib pkgs; };

  right-of = name:
    let display = displays.${name};
    in {
      x = display.position.x + display.mode.x / display.scale;
      # y = (display.position.y + display.mode.y) / display.scale;
      y = 0;
    };

  displays = {
    "BenQ Corporation BenQ PD2700U ETB8L04005SL0" = {
      mode = {
        x = 3840;
        y = 2160;
        hz = "59.997";
      };
      scale = 1.5;

      position = right-of "Samsung Electric Company LS27A800U HCJR800036";
    };
    "Samsung Electric Company LS27A800U HCJR800036" = {
      mode = {
        x = 3840;
        y = 2160;
        hz = "59.997";
      };
      scale = 1.5;

      position = right-of "BOE 0x0BCA Unknown";
    };

    # Framework laptop display with matte screen
    "BOE 0x0BCA Unknown" = {
      mode = {
        x = 2256;
        y = 1504;
        hz = "59.999";
      };
      scale = 1.5;

      position.x = 0;
      position.y = 0;
    };

    "Unknown Wacom One 13 1JW0171005549" = {
      mode = {
        x = 1920;
        y = 1080;
        hz = "60.000";
      };

      scale = 1.0;

      position = right-of "BOE 0x0BCA Unknown";
    };
  };

  outputLine = name:
    { mode, position, scale }:
    ''
      output "${name}" position ${toString (builtins.floor position.x)} ${
        toString (builtins.floor position.y)
      } mode "${toString (builtins.floor mode.x)}x${
        toString (builtins.floor mode.y)
      }@${mode.hz}Hz" scale ${toString scale}'';
  outputLines = concatStringsSep "\n" (mapAttrsToList outputLine displays);
in {
  imports = [
    ./waybar-datum.nix
    # ./rofi-datum.nix
    ./gtk-tiling.nix
  ];

  services.mako = mkIf config.wayland.windowManager.sway.enable {
    enable = true;

    borderRadius = 4;
    defaultTimeout = 2500;
    font = "Monofur NerdFont";
    layer = "overlay";

    textColor = "${wellknown.colors.foreground}FF";
    backgroundColor = "${wellknown.colors.background}B0";
  };

  home.packages = with pkgs; [ xdg-desktop-portal-wlr ];

  gtk.enable = mkDefault true;

  wayland.windowManager.sway = mkIf nixosConfig.programs.sway.enable {
    enable = true;

    wrapperFeatures.gtk = mkDefault config.gtk.enable;

    config = null;

    extraConfig = let
      pointerCursor =
        config.home.pointerCursor or config.xsession.pointerCursor;
    in ''
      exec_always systemctl --user restart waybar
      exec dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP=sway

      set $foreground ${wellknown.colors.foreground} 
      set $background ${wellknown.colors.background}

      default_border pixel 2

      ${import ./i3-datum/vars.nix { inherit pkgs config nixosConfig; }}

      output * background ${config.home.homeDirectory}/Pictures/.backgrounds/01.jpeg fill

      # Main desk displays
      focus output 'Samsung Electric Company LS27A800U HCJR800036'

      ${outputLines}

      seat * xcursor_theme "${pointerCursor.name} ${
        toString pointerCursor.size
      }"

      include ${wellknown.i3.configFile}
      include ${./i3-datum/i3config}
      include ${./i3-datum/keybinds.i3config}
      include ${./i3-datum/media.keybinds.i3config}

      for_window [ instance="^civilizationv_.+\.exe$"] floating enable; resize set 1440px 900px; move position center

      input "2362:628:PIXA3854:00_093A:0274_Touchpad" dwt enable
      input "2362:628:PIXA3854:00_093A:0274_Touchpad" dwt enable

      input "1386:934:Wacom_One_Pen_Display_13_Pen" map_to_output "Unknown Wacom One 13 1JW0171005549"
    '';
  };
}
