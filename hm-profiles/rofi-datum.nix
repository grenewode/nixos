{ config, lib, ... }:
with lib; {
  programs.rofi = mkIf config.programs.rofi.enable {
    extraConfig = {
      "markup" = "";
      "modi" = [ "run" "drun" ];
    };
  };
}
