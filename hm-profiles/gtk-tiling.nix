{ config, lib, pkgs, ... }: {
  gtk = {
    gtk3 = {
      extraConfig = { gtk-decoration-layout = "menu:"; };

      extraCss = ''
        /* remove window title from Client-Side Decorations */
        .solid-csd headerbar .title {
            font-size: 0;
        }

        /* hide extra window decorations/double border */
        window decoration {
            margin: 0;
            border: none;
            padding: 0;
        }
      '';
    };
  };
}
