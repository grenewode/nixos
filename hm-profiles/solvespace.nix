{ pkgs, config, lib, self, ... }:
with lib; {
  imports = [ ];

  programs.solvespace = {
    enable = true;

    settings = { UseSIPrefixes = true; };
  };
}
