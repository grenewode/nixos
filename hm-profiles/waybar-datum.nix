{ config, lib, pkgs, nixosConfig, ... }:
with lib; {
  programs.waybar = {
    enable = true;
    systemd.enable = true;

    settings = let
      isDesktop = builtins.elem "desktop" nixosConfig.system.nixos.tags;
      isLaptop = !isDesktop;
      modules = {
        "sway/workspaces" = {
          disable-scroll = true;
          all-outputs = true;
        };

        temperature = {
          interval = 1;

          hwmon-path = if nixosConfig.networking.hostName == "terra" then
            "/sys/devices/platform/coretemp.0/hwmon/hwmon4/temp1_input"
          else if nixosConfig.networking.hostName == "mercury" then
            "/sys/devices/platform/coretemp.0/hwmon/hwmon5/temp1_input"
          else
            "";

          format = "THM {temperatureC:03d} °C";
        };

        cpu = {
          interval = 1;
          format = "CPU {usage:03d}% ({avg_frequency:03.2} GHz)";
        };

        "custom/gpu" = {
          exec = if nixosConfig.networking.hostName == "terra" then
            "cat /sys/class/drm/card0/device/gpu_busy_percent"
          else if nixosConfig.networking.hostName == "mercury" then
            "cat /sys/devices/pci0000:00/0000:00:07.2/0000:56:00.0/0000:57:01.0/0000:58:00.0/0000:59:00.0/0000:5a:00.0/gpu_busy_percent"
          else
            "";
          format = "GPU {:0>3}%";
          interval = 1;
        };

        "network/lan" = {
          interval = 1;

          interface = "enp*";

          format = "LAN  ⭱{bandwidthUpBits:>8} ⭳{bandwidthDownBits:<8}";
          format-disconnected = "LAN";
        };

        "network/wlan" = {
          interval = 1;
          interface = "wlp*";
          format = "WLAN ⭱{bandwidthUpBits:>8} ⭳{bandwidthDownBits:<8}";
          format-disconnected = "WLAN";
        };

        memory = {
          interval = 1;
          format = "MEM {used:04.1f}/{total:02.0f} GiB";
        };

        battery = mkIf (isLaptop) {
          format = "PWR[=] {capacity:03}% ({time:>5} {power:03.0f} W)";
          format-discharging = "PWR[-] {capacity:03}% ({time} {power:03.0f} W)";
          format-charging = "PWR[+] {capacity:03}% ({time} {power:03.0f} W)";
          format-time = "{H:02}:{M:02}";

          states = {
            warning = 15;
            critical = 10;
          };
        };

        pulseaudio = {
          format = "VOL {volume:03d}% {format_source}";
          format-muted = "VOL MUTE {format_source}";

          format-source = "SRC {volume:03d}%";
          format-source-muted = "SRC MUTE";

          tooltip = true;

          format-icons = {
            headphone = "HEAD";
            hdmi = "DISP";
            speaker = "SPKR";
            hifi = "HIFI";
            hands-free = "HNDF";
            headset = "HDST";
            phone = "PHNE";
            portable = "PORT";
            car = "AUTO";
            default = "DFLT";
          };
        };

        clock = {
          interval = 1;
          format = "{:%R %% %F}";
          tooltip = true;
          tooltip-format = "{:%c}";
        };
      };

    in [
      ({
        layer = "top";
        position = "top";
        output = [
          "DP-1"
          "DP-2"
          "DP-3"
          "DP-4"
          "DP-5"
          "DP-6"
          "DP-7"
          "DP-8"
          "DP-9"
          "eDP-1"
        ];

        modules-left = [ "sway/workspaces" ];
        modules-center = [
          "network/lan"
          "network/wlan"
          "temperature"
          "cpu"
          "custom/gpu"
          "memory"
        ];
        modules-right = if isDesktop then [
          "pulseaudio"
          "clock"
        ] else [
          "battery"
          "pulseaudio"
          "clock"
        ];

      } // (if (config.home.stateVersion <= "21.11") then {
        inherit modules;
      } else
        modules))
    ];

    style = ''
      * {
          border: none;
          border-radius: 0;
          min-height: 0;
          transition: inherit;
          font-family: inherit;
          font-size: inherit;
      }

      window {
          background: alpha(@theme_bg_color, 0.5);
          padding: 0 8px;

          font-family: "BlexMono Nerd Font";
      }

      window.eDP-1 {
        font-size: 10px;
      }

      widget > * {
        padding: 0 8px;
      }

      window > box > box {
        background: @theme_base_color;
        color: @theme_fg_color;
        padding: 0 8px;
      }

      .modules-left > widget:first-child > * {
        margin-left: 0;
      }

      .modules-right > widget:last-child > * {
        margin-right: 0;
      }

      @keyframes battery-warning {
        to {
          background-color: @warning_color;
        }
      }


      @keyframes battery-critical {
        to {
          background-color: @error_color;
        }
      }

      #battery.warning:not(.charging) {
        animation: battery-warning 2s infinite alternate;
      }

      #battery.critical:not(.charging) {
        animation: battery-critical 2s infinite alternate;
      }

      #battery.charging {
          background-color: mix(@success_color, @theme_base_color, 0.5);
      }

      #workspaces button {
          padding: 0 8px;
          background: transparent;
          color: inherit;
          transition: box-shadow 0.5s;
      }

      #workspaces button.focused {
          box-shadow: inset 0px -6px 1px -2px @theme_fg_color;
      }

      #workspaces button.visible:not(.focused) {
          box-shadow: inset 0px  -4px 1px -2px @theme_fg_color;
      }
    '';

  };
}
