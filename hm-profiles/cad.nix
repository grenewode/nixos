{ pkgs, config, lib, self, ... }:
with lib; {
  imports = [ "${self}/hm-profiles/solvespace.nix" ];

  home.packages = with pkgs; [ prusa-slicer openscad ];

  home.state.directories = [
    "${config.xdg.configHome}/PrusaSlicer/"
    "${config.xdg.dataHome}/OpenSCAD"
    "${config.xdg.configHome}/OpenSCAD"
  ];
}
