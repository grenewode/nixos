{ pkgs, config, lib, options, ... }:
with lib; {
  programs.beets = mkIf config.programs.beets.enable {
    package =
      (pkgs.beets.override { enableCopyArtifacts = true; }).overrideAttrs
      (attrs: {
        propagatedBuildInputs = attrs.propagatedBuildInputs
          ++ [ pkgs.beetcamp ];
      });

    settings = rec {
      directory = "${config.home.homeDirectory}/Music";
      library = "${directory}/beets.library.db";
      plugins =
        [ "fetchart" "discogs" "deezer" "beetcamp" "fromfilename" "missing" ];
    };
  };

  home.state.files = mkIf (config.programs.beets.enable) [
    "${config.xdg.configHome}/beets/discogs_token.json"
    "${config.xdg.configHome}/beets/state.pickle"
  ];
}
