{ self, config, pkgs, lib, ... }:
with lib; {

  imports = [ "${self}/hm-profiles/rofi-datum.nix" ];

  programs.rofi.enable = true;

  xsession.windowManager.qtile = {
    enable = true;

    config = ''
      from typing import List  # noqa: F401

      from libqtile import bar, layout, widget, hook
      from libqtile.config import Click, Drag, Group, Key, Match, Screen
      from libqtile.lazy import lazy
      from libqtile.utils import guess_terminal

      mod = "mod4"
      terminal = "${pkgs.alacritty}/bin/alacritty"
      menu = "${config.programs.rofi.package}/bin/rofi -show drun"

      keys = [
          # A list of available commands that can be bound to keys can be found
          # at https://docs.qtile.org/en/latest/manual/config/lazy.html
          # Switch between windows
          Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
          Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
          Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
          Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
          Key([mod], "Tab", lazy.layout.next(), desc="Move window focus to other window"),
          Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
          Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
          Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
          Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),
          Key([mod, "shift"], "Return", lazy.spawn(terminal), desc="Launch terminal"),
          Key([mod], "Return", lazy.spawn(menu), desc="Launch rofi launcher"),
          Key([mod], "q", lazy.window.kill(), desc="Kill focused window"),
          Key([mod, "shift"], "r", lazy.reload_config(), desc="Reload the config"),
          Key([mod, "shift"], "q", lazy.shutdown(), desc="Shutdown Qtile"),

          Key([mod], "Right", lazy.screen.next_group()),
          Key([mod], "Left", lazy.screen.prev_group()),
      ]

      groups = [Group(i) for i in "123456789"]

      for i in groups:
          keys.extend(
              [
                  # mod1 + letter of group = switch to group
                  Key(
                      [mod],
                      i.name,
                      lazy.group[i.name].toscreen(),
                      desc="Switch to group {}".format(i.name),
                  ),
                  # mod1 + shift + letter of group = switch to & move focused window to group
                  Key(
                      [mod, "shift"],
                      i.name,
                      lazy.window.togroup(i.name, switch_group=True),
                      desc="Switch to & move focused window to group {}".format(i.name),
                  ),
                  # Or, use below if you prefer not to switch to that group.
                  # # mod1 + shift + letter of group = move focused window to group
                  # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
                  #     desc="move focused window to group {}".format(i.name)),
              ]
          )

      layouts = [
          # layout.Columns(border_focus_stack=["#d75f5f", "#8f3d3d"], border_width=4),
          # layout.Max(),
          # Try more layouts by unleashing below layouts.
          # layout.Stack(num_stacks=2),
          # layout.Bsp(),
          # layout.Matrix(),
          layout.MonadTall(),
          layout.MonadWide(),
          layout.RatioTile(),
          # layout.Tile(),
          # layout.TreeTab(),
          # layout.VerticalTile(),
          # layout.Zoomy(),
      ]

      widget_defaults = dict(
          font="${config.gtk.font.name}",
          padding=16,
      )
      extension_defaults = widget_defaults.copy()

      screens = [
          Screen(
              top=bar.Bar(
                  [
                      widget.CurrentLayout(),
                      widget.GroupBox(),
                      widget.WindowName(),
                      widget.Chord(
                          chords_colors={
                              "launch": ("#ff0000", "#ffffff"),
                          },
                          name_transform=lambda name: name.upper(),
                      ),
                      widget.Systray(),
                      widget.Clock(format="%F %% %R"),
                      widget.QuickExit(),
                  ],
                  24 
                  # border_width=[2, 0, 2, 0],  # Draw top and bottom borders
                  # border_color=["ff00ff", "000000", "ff00ff", "000000"]  # Borders are magenta
              ),
          ),
      ]

      @hook.subscribe.client_new
      def transient_window(window):
          if window.window.get_wm_transient_for():
              window.floating = True

      # Drag floating layouts.
      mouse = [
          Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
          Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
          Click([mod], "Button2", lazy.window.bring_to_front()),
      ]

      dgroups_key_binder = None
      dgroups_app_rules = []  # type: List
      follow_mouse_focus = True
      bring_front_click = False
      cursor_warp = False
      floating_layout = layout.Floating(
          float_rules=[
              # Run the utility of `xprop` to see the wm class and name of an X client.
              *layout.Floating.default_float_rules,
              Match(wm_class="Yubico Authenticator"),
              Match(wm_class="confirmreset"),  # gitk
              Match(wm_class="makebranch"),  # gitk
              Match(wm_class="maketag"),  # gitk
              Match(wm_class="ssh-askpass"),  # ssh-askpass
              Match(title="branchdialog"),  # gitk
              Match(title="pinentry"),  # GPG key password entry
          ]
      )
      auto_fullscreen = True
      focus_on_window_activation = "smart"
      reconfigure_screens = True

      # If things like steam games want to auto-minimize themselves when losing
      # focus, should we respect this or not?
      auto_minimize = True

      # XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
      # string besides java UI toolkits; you can see several discussions on the
      # mailing lists, GitHub issues, and other WM documentation that suggest setting
      # this string if your java app doesn't work correctly. We may as well just lie
      # and say that we're a working one by default.
      #
      # We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
      # java that happens to be on java's whitelist.
      wmname = "LG3D"
    '';
  };
}
