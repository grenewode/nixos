{ config, lib, pkgs, ... }:
with lib; {
  home.packages = with pkgs;
    ((lists.optional config.services.gpg-agent.enable pinentry-gtk2));

  services.gpg-agent = mkIf config.programs.gpg.enable {
    enable = true;

    enableExtraSocket = true;
    enableScDaemon = true;
    enableSshSupport = true;

    defaultCacheTtl = 3600;
    defaultCacheTtlSsh = 3600;

    pinentryFlavor = "gtk2";
  };
}
