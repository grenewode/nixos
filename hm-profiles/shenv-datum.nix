{ config, options, pkgs, lib, nixosConfig, ... }:
with lib; {
  home.state.directories = with config.programs;
    ((lists.optional direnv.enable "${config.xdg.dataHome}/direnv")
      ++ (lists.optional bash.enable (builtins.dirOf bash.historyFile)));

  home.state.files = with config.programs;
    ((lists.optional zsh.enable zsh.history.path));

  programs.zsh = rec {
    enable = nixosConfig.programs.zsh.enable;
    enableCompletion = true;
    enableAutosuggestions = true;
    enableVteIntegration = true;
    enableSyntaxHighlighting = true;
    autocd = true;

    dotDir = strings.removePrefix "${config.home.homeDirectory}/"
      "${config.xdg.configHome}/zsh";

    history = {
      ignoreDups = true;
      path = "${config.xdg.dataHome}/zsh/history";
    };

    # oh-my-zsh = {
    #   enable = true;
    #   plugins = [ "git" "sudo" ];
    # };
  };

  programs.bash = {
    enable = true;
    enableVteIntegration = true;

    historyFile = "${config.xdg.dataHome}/bash/history";
  };

  programs.direnv = {
    enable = true;
    nix-direnv.enable = true;
    nix-direnv.enableFlakes = mkIf (config.home.stateVersion <= "21.11") true;

    stdlib = ''
      layout_postgres() {
          export PGDATA="$(direnv_layout_dir)/postgres"
          export PGHOST="$PGDATA"
          export PGPORT="''${PGPORT:-5431}"

          if [[ ! -d "$PGDATA" ]]; then
          initdb
          cat >> "$PGDATA/postgresql.conf" <<-EOF
              listen_addresses = '''
              unix_socket_directories = '$PGHOST'
      EOF
          echo "CREATE DATABASE $USER;" | postgres --single -E postgres
          fi
      }

      layout_todotxt() {
          export TODO_DIR="$(direnv_layout_dir)/todo/todo"
          export TODOTXT_CFG_FILE="$(direnv_layout_dir)/todo/config.cfg"

          mkdir -p "$(dirname "$TODOTXT_CFG_FILE")"
          mkdir -p "$TODO_DIR"

          cat << EOF > "$TODOTXT_CFG_FILE"
      TODO_DIR="$TODO_DIR"
      EOF

          todo.sh list
      }

      layout_poetry() {
          PYPROJECT_TOML="''${PYPROJECT_TOML:-pyproject.toml}"
          if [[ ! -f "$PYPROJECT_TOML" ]]; then
              log_status "No pyproject.toml found. Executing \`poetry init\` to create a \`$PYPROJECT_TOML\` first."
              poetry init
          fi

          if [[ -d ".venv" ]]; then
              VIRTUAL_ENV="$(pwd)/.venv"
          else
              VIRTUAL_ENV=$(poetry env info --path 2>/dev/null ; true)
          fi

          if [[ -z "$VIRTUAL_ENV" || ! -d "$VIRTUAL_ENV" ]]; then
              log_status "No virtual environment exists. Executing \`poetry install\` to create one."    
              poetry install
              VIRTUAL_ENV=$(poetry env info --path)
          fi

          PATH_add "$VIRTUAL_ENV/bin"
          export POETRY_ACTIVE=1
          export VIRTUAL_ENV
      }
    '';
  };
}
