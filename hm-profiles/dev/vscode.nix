{ pkgs, config, lib, ... }: {

  programs.vscode = lib.mkIf config.programs.vscode.enable {
    enableExtensionUpdateCheck = false;
    enableUpdateCheck = false;
    extensions = with pkgs.vscode-extensions;
      (lib.optionals (config.programs.direnv.enable) [ mkhl.direnv ])
      ++ ([ esbenp.prettier-vscode editorconfig.editorconfig bbenoist.nix ]);

    haskell = lib.mkIf (pkgs ? hie-nix) { enable = true; };
  };

}
