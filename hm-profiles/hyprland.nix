{ config, lib, pkgs, nixosConfig, ... }:
let
  inherit (nixosConfig.programs) hyprland;
  inherit (lib) mkIf mkDefault;
  hyper = "SUPER + SHIFT";
  meh = "SUPER";
in {

  config = mkIf hyprland.enable {
    xdg.configFile."hypr/hyprland.conf" = {
      text = ''

      bind = ${meh} , 1, workspace      , 1
      bind = ${meh} , 2, workspace      , 2
      bind = ${meh} , 3, workspace      , 3
      bind = ${meh} , 4, workspace      , 4
      bind = ${meh} , 5, workspace      , 5
      bind = ${meh} , 6, workspace      , 6
      bind = ${meh} , 7, workspace      , 7
      bind = ${meh} , 8, workspace      , 8
      bind = ${meh} , 9, workspace      , 9

      bind = ${hyper}, 1, movetoworkspace, 1
      bind = ${hyper}, 2, movetoworkspace, 2
      bind = ${hyper}, 3, movetoworkspace, 3
      bind = ${hyper}, 4, movetoworkspace, 4
      bind = ${hyper}, 5, movetoworkspace, 5
      bind = ${hyper}, 6, movetoworkspace, 6
      bind = ${hyper}, 7, movetoworkspace, 7
      bind = ${hyper}, 8, movetoworkspace, 8
      bind = ${hyper}, 9, movetoworkspace, 9

      bind = ${meh}   , Return, exec, ${pkgs.wofi}/bin/wofi -show drun
      bind = ${hyper} , Return, exec, ${pkgs.foot}/bin/foot
'';
    };
  };

}
