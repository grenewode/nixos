{ pkgs, config, nixosConfig }:
(''
  set $visible_to_workspace ${pkgs.grenewode.i3switcher}/bin/i3switcher
  set $container_to_workspace ${pkgs.grenewode.i3switcher}/bin/i3switcher --move-container
  set $terminal ${config.programs.foot.package}/bin/foot
  set $pactl ${nixosConfig.hardware.pulseaudio.package}/bin/pactl
  set $brightnessctl ${pkgs.brightnessctl}/bin/pactl
'' + (if !config.wayland.windowManager.sway.enable then ''
  set $menu ${config.programs.rofi.package}/bin/rofi -show drun
'' else ''
  set $menu ${pkgs.wofi}/bin/wofi --show drun
''))
