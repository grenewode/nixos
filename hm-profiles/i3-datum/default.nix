{ config, pkgs, lib, nixosConfig, ... }:
with lib;
let
  wellknown = import ../../lib/wellknown.nix { inherit lib pkgs; };
  cfg = config.xsession.windowManager.i3;
in {
  imports = [ ../gtk-tiling.nix ../rofi-datum.nix ];

  xsession.windowManager.i3 = {
    inherit (nixosConfig.services.xserver.windowManager.i3) enable package;

    config = null;

    extraConfig = ''
      set_from_resource $fg i3wm.color7 #f0f0f0
      set_from_resource $bg i3wm.color2 #f0f0f0

      ${import ./vars.nix { inherit pkgs config nixosConfig; }}
      ${wellknown.i3.extraConfig}
      ${readFile ./i3config}
      ${readFile ./keybinds.i3config}
      ${readFile ./media.keybinds.i3config}
    '';
  };
}

