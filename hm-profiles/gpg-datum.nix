{ config, lib, options, ... }:
with lib; {
  programs.gpg = {
    enable = mkDefault true;
    homedir = "${config.xdg.configHome}/gnupg";
  };

  home.state.directories = mkIf (options.home ? state)
    (with config.programs; lists.optional gpg.enable gpg.homedir);
}
