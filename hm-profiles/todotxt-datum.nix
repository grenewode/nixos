{ pkgs, lib, config, ... }:
let
  todotxtPlugin = { name, bins ? [ name ], src }:
    (pkgs.stdenv.mkDerivation {
      inherit name src;

      buildPhase = ''
        mkdir -p $out
        cp --recursive $src/. $out
        ${lib.strings.concatMapStrings (bin: ''
          chmod +x $out/${bin}
        '') bins}
      '';

      dontInstall = true;
    });
  date-add = todotxtPlugin {
    name = "add";

    src = pkgs.fetchFromGitHub {
      owner = "DoctorRadar";
      repo = "todotxt_cli_date_add";

      rev = "master";
      hash = "sha256-2kQj0tYfvZVpt4D5A1ob2qx8o5LyqMve8G/h/QKXImg=";
    };

  };
  due = todotxtPlugin {
    name = "due";

    src = pkgs.fetchFromGitHub {
      owner = "rebecca-owen";
      repo = "due";

      rev = "master";
      hash = "sha256-c7W1R8r84LJ3cyVgzmx+eprw4LcXZK+Y/8UIIt6qGkE=";
    };
  };
in {
  home.packages = [ pkgs.todo-txt-cli ];

  xdg.configFile."todo.txt/config.cfg".text = ''
    export TODO_DIR=$HOME/Documents/Sync/Notebook
    export TODO_NOTE_EXT=.md
  '';

  xdg.configFile."todo.txt/actions/add" = { source = date-add; };

  xdg.configFile."todo.txt/actions/due" = { source = due; };

  home.sessionVariables = {
    TODO_ACTIONS_DIR = "${config.xdg.configHome}/todo.txt/actions";
    TODOTXT_CFG_FILE = config.xdg.configFile."todo.txt/config.cfg".source;
  };

  programs.zsh.shellAliases."tt" = "${pkgs.todo-txt-cli}/bin/todo.sh";
  programs.bash.shellAliases."tt" = "${pkgs.todo-txt-cli}/bin/todo.sh";
  programs.fish.shellAliases."tt" = "${pkgs.todo-txt-cli}/bin/todo.sh";
}
