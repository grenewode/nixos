{ pkgs, lib, config, self, ... }:
with lib; {
  xdg.portal = {
    enable = !config.services.xserver.desktopManager.gnome.enable;
    extraPortals = with pkgs; [ xdg-desktop-portal-gtk xdg-desktop-portal-wlr ];
    wlr = { enable = true; };
  };
}
