{ config, pkgs, lib, self, options, ... }:
with lib; {
  virtualisation = {
    docker.enable = mkDefault true;

    libvirtd = {
      enable = mkDefault true;
      qemu.runAsRoot = false;
      qemu.ovmf = {
        enable = true;
        package = pkgs.OVMFFull;
      };
    };

    spiceUSBRedirection.enable =
      mkDefault config.virtualisation.libvirtd.enable;

    containers.enable = mkDefault true;

    lxc = {
      enable = mkDefault true;
      lxcfs.enable = mkDefault config.virtualisation.lxc.enable;
    };

  };

  environment.state = {
    directories =
      (optionals config.virtualisation.docker.enable [ "/var/lib/docker" ])
      ++ (optionals config.virtualisation.libvirtd.enable [
        "/var/lib/libvirt/images"
        "/var/lib/libvirt/secrets"
        "/var/lib/libvirt/storage"
        "/var/lib/libvirt/qemu"
        "/var/lib/pki/libvirt"
      ]) ++ (optionals config.virtualisation.lxd.enable [ "/var/lib/lxd" ])
      ++ (optionals config.virtualisation.containers.enable
        [ "/var/lib/containers/storage" ]) ++ [ "/var/lib/pki/CA" ];
  };

  home-manager.sharedModules = [
    ({ config, lib, nixosConfig, ... }: {

      programs.virt-manager = {
        enable = lib.mkDefault nixosConfig.virtualisation.libvirtd.enable;

        connections = {
          "qemu:///system" = { autoconnect = true; };
          "qemu:///session" = { autoconnect = true; };
        };
      };

      home.state.directories = (optionals
        (nixosConfig.virtualisation.libvirtd.enable
          && !nixosConfig.virtualisation.libvirtd.qemu.runAsRoot
          && builtins.elem "libvirtd"
          nixosConfig.users.users.${config.home.username}.extraGroups) [
            ".pki/libvirt"
            (strings.removePrefix config.home.homeDirectory
              "${config.xdg.configHome}/libvirt")
            (strings.removePrefix config.home.homeDirectory
              "${config.xdg.dataHome}/libvirt/images")
          ]);
    })
  ];
}
