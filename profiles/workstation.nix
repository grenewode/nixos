{ config, pkgs, lib, self, ... }:
with lib; {
  imports = [
    "${self}/lib/base-config.nix"
    "${self}/profiles/personal-computer"
  ];

  services.sshd.enable = true;

  environment.systemPackages = (with pkgs; [
    git
    fzf
    usbutils
    tree
    ripgrep
    jq
    pciutils
    unzip
    jdupes
    killall
    aria2
    pv
    ranger
    pavucontrol
    bind
    # chromiumBeta
  ]);

  programs.neovim.enable = true;

  hardware.spacemouse.enable = true;

  hardware.sane = {
    enable = true;

    # extraBackends = [ pkgs.utsushi ];
  };

  programs.zsh = { enable = true; };

  programs.wireshark = {
    enable = true;
    package = mkIf (config.services.xserver.enable
      || config.programs.sway.enable
      || config.services.xserver.displayManager.gdm.wayland) pkgs.wireshark;
  };

  programs.adb.enable = true;
  services.udev.packages = with pkgs; [ android-udev-rules ];

  nixpkgs.config.allowUnfree = true;
}
