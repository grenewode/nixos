{ self, config, lib, pkgs, nixpkgs, ... }: {

  imports = [
    "${nixpkgs}/nixos/modules/profiles/hardened.nix"
    "${nixpkgs}/nixos/modules/profiles/headless.nix"
    "${nixpkgs}/nixos/modules/profiles/minimal.nix"
    "${self}/lib/base-config.nix"
  ];

  services.clamav.daemon.enable = true;
  services.clamav.updater.enable = true;

  programs.mosh.enable = true;

  services.openssh = {
    enable = true;

    forwardX11 = false;
    logLevel = "VERBOSE";
    passwordAuthentication = false;
    permitRootLogin = "no";

    startWhenNeeded = true;
  };

  security.sudo = {
    enable = true;

    execWheelOnly = true;
    wheelNeedsPassword = true;
  };

  users = {
    mutableUsers = false;

    groups.sysadmin = { };

    users = {
      root.hashedPassword = "*";
      sysadmin = {
        isNormalUser = true;
        group = "sysadmin";

        extraGroups = [ "wheel" ];

        hashedPassword =
          "$6$mlbZgX0OoA1.ympw$TVE.mG22nkDNv.QIZ.ZW5H8YvYgCm7AtXiKBEqFN.48i4LlItvZ3ie3xAx2HeXgFQdH.TrEfGQQaMPt8cgqZA.";
        openssh.authorizedKeys.keys = [
          "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDG+CTN3wwIiErQBwNDUUEx0VNromjEsDFp8N6y5x2U/nwOE05jC9NjKwf8MyM8f0mDqJfLAcfv2+tyQPP08ndtxSDwfCY0wFFcsVraksR84AULpCwJFuRJVV86O/S1Aat9n4iEMATdx/GSMWce1SnOpezanja/b43tliFN7OHfFsPgFKG+ojP9bh+bFu7B4xH8edFgMEbQAUHIqwb3xA00JW5l7h1wx/2QaGc+ucMwPgkxoubVE+O9Anio2Gwnu0nR4akBgEGXbwR5sUzV6DuiMAg/GRSHzeCiPc5NHEC7MOTPrIQh0x+j+triBebCw/ec95FRNlhXIMPiqoZhuGlIBTTm5uO18nZuSZ0cbL2pxDNBVk2ZBB3FNxiJ3JntzleNYk+K4EtFjvT2XBqwUNTvsuxJe6bvM0dvFvY9/tMjJJGpsCsxR9PWdUtHQO93JsQK1gld2lUA1c+JAdMMy13q4HfKe8yQGDw3D0qXYoAvy52RZlrXcwsOsHC1UqXZdaZaZ5xLtviLrtSTXwqg/3edq4uuuouezNzOUeY3JQ3wIOCahYs/wsQ+x9M2xWBzM6ZUfqtI8J0HwtRAm+9JfMkxYPEiLvoC8f8C4YZPgWg5JUlSu0f2/+i0dV4bddxPdjEGIdIp6j2DC8WK6CrR6Ve84FJr3UP4AF6vSq4MfyD54Q== cardno:000611467797"
        ];
      };
    };
  };

  # Some settings to make sure that the nix store doesn't get too big
  nix = {
    gc = {
      automatic = true;
      dates = "weekly";
    };

    autoOptimiseStore = true;
    optimise = {
      automatic = true;
      dates = [ "weekly" ];
    };
  };
}
