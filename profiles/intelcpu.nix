{ config, lib, pkgs, ... }: {
  boot.kernelModules =
    lib.mkIf config.virtualisation.libvirtd.enable [ "kvm-intel" ];
}
