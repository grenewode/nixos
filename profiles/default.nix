{ config, lib, helpers, ... }:

{
  imports = (import ../../lib/submodules.nix {
    root = ./.;
    exclude = "default\\.nix";
  });
}
