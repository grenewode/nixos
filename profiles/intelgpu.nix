{ pkgs, lib, config, ... }: {

  # https://discourse.nixos.org/t/hardware-accelerated-opengl-with-intel-onboard-card/10643
  # services.xserver.videoDrivers = [ ]; # modesetting didn't help

  hardware.opengl = lib.mkIf (config.hardware.opengl.enable) {
    driSupport = true;
    extraPackages = with pkgs; [
      intel-media-driver # LIBVA_DRIVER_NAME=iHD
      vaapiIntel # LIBVA_DRIVER_NAME=i965 (older but works better for Firefox/Chromium)
      vaapiVdpau
      libvdpau-va-gl
    ];
  };
}
