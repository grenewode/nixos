{ pkgs, lib, config, self, ... }:
with lib; {
  imports = [
    ./bash.nix
    "${self}/lib/base-config.nix"
    "${self}/profiles/pipewire.nix"
    "${self}/profiles/xdg-desktop.nix"
  ];

  nixpkgs = { config.allowUnfree = true; };

  boot = {
    initrd = { kernelModules = config.services.xserver.videoDrivers; };

    binfmt = {
      emulatedSystems = builtins.filter (platform:
        strings.hasSuffix "linux" platform && platform != pkgs.system)
        self.inputs.flake-utils.lib.defaultSystems;
    };

    supportedFilesystems = [ "ntfs" "nfs" ];
  };

  environment.systemPackages =
    optionals config.hardware.openrazer.enable [ pkgs.razergenie ];

  users = {
    mutableUsers = false;
    users.root.hashedPassword = "*";
  };

  hardware = {
    enableRedistributableFirmware = true;
    openrazer = {
      enable = true;
      devicesOffOnScreensaver = true;
      users = [ "grenewode" ];
    };

    opengl = {
      enable = true;
      driSupport = true;
      driSupport32Bit = true;
    };

    bluetooth.enable = mkDefault true;

    yubico.enable = true;
  };

  programs = {
    dconf.enable = true;
    fuse.userAllowOther = true;
    light.enable = true;
  };

  security = { polkit.enable = true; };

  services = {

    greetd = {
      enable = true;
      restart = true;

      settings = {
        default_session = {
          command = "${pkgs.greetd.tuigreet}/bin/tuigreet --time --cmd sway";
        };
      };
    };

    # xserver = {
    #   enable = true;

    #   # displayManager.gdm = {
    #   #   enable = true;
    #   #   wayland = true;
    #   # };

    #   displayManager.lightdm = {
    #     enable = true;

    #     greeters.gtk = { enable = true; };
    #   };

    #   # displayManager.sddm = {
    #   #   enable = true;
    #   #   enableHidpi = true;
    #   # };
    # };
    actkbd = mkIf config.programs.light.enable {
      enable = true;
      bindings = [
        {
          keys = [ 224 ];
          events = [ "key" ];
          command = "${pkgs.light}/bin/light -A 10";
        }
        {
          keys = [ 225 ];
          events = [ "key" ];
          command = "${pkgs.light}/bin/light -U 10";
        }
      ];
    };

    printing = {
      enable = mkDefault true;

      drivers = with pkgs; [
        gutenprint
        cnijfilter2
        cups-bjnp
        carps-cups
        gutenprintBin
      ];

      
    };

    xserver.libinput.enable = true;
  };

  networking = {
    useDHCP = false;
    domain = mkDefault "lair.onl";
    useNetworkd = mkDefault false;

    networkmanager = mkDefault { enable = true; };
  };

  home-manager = {
    useUserPackages = true;
    useGlobalPkgs = true;
    verbose = true;
    backupFileExtension = "bk";

    sharedModules = [
      ({ config, ... }: {
        home.stateVersion = "22.11";
        home.enableNixpkgsReleaseCheck = true;
        home.extraOutputsToInstall = [ "doc" "info" "devdoc" ];
        home.state.directories = with config.xdg.userDirs; [
          documents
          music
          pictures
          publicShare
          videos
        ];

        programs.firefox.enable = mkDefault true;

        xdg = with config.home; {
          enable = true;

          cacheHome = "${homeDirectory}/.cache";
          configHome = "${homeDirectory}/.config";
          dataHome = "${homeDirectory}/.local/share";

          userDirs = {
            enable = true;

            desktop = "${homeDirectory}/Desktop";
            documents = "${homeDirectory}/Documents";
            pictures = "${homeDirectory}/Pictures";
            videos = "${homeDirectory}/Videos";
            music = "${homeDirectory}/Music";
            download = "${homeDirectory}/Downloads";
            publicShare = "${homeDirectory}/Public";
            templates = "${homeDirectory}/Templates";
          };
        };

      })
    ];

    extraSpecialArgs = { inherit self; };
  };

}
