{ config, lib, pkgs, ... }:
with lib;
let
  git-prompt = pkgs.fetchurl {
    url =
      "https://raw.githubusercontent.com/git/git/master/contrib/completion/git-prompt.sh";
    hash = "sha256-RT4WfKexlcqEIsxsgMMUkcM/VOenEdQoeTQBOEWtCGc=";
  };
in {
  programs.bash = {
    enableLsColors = true;
    enableCompletion = true;
    undistractMe = {
      enable = true;
      playSound = false;
      timeout = 60;
    };
    vteIntegration = true;

    promptInit = let
      ps1 = pkgs.writeScript "ps1" ''
        #! /usr/bin/env bash
        (
          result=$?

          source "${git-prompt}"

          __styled_text() {
            local style=$1
            shift
            printf "\e[%sm%s\e[0m" "$style" "$@"
          }

          __todotxt_ps1() {
            [[ "$TODOTXT_CFG_FILE" == "" ]] && exit
            (
              { . "$TODOTXT_CFG_FILE"; } || exit
              [[ ! -d "$TODO_DIR" ]] && exit

              TODO_FILES="$(find "$TODO_DIR" -type f ! -name "*.bak" ! -name "report*" -print)"

              COUNT="$(cat $TODO_FILES | sed "/^x.*/d" | wc -l)"

              printf "(todo.txt: %d)" "$COUNT"
            )
          }

          __ps1_utilities_sep() {
            local text="$@"

            [[ "$text" == "" ]] && exit
            printf " %s" "$text"
          }

          HNT="\e[2"
          ATN="\e[1"

          OK=";32m"
          NR="m"
          ERR=";31m"

          RST="\e[0m"

          git_ps1="$(__git_ps1)"
          todotxt_ps1="$(__todotxt_ps1)"

          status1=()
          hour=$(date +"%H")
          if [[ $hour -lt 6 ]];
          then
            status1+=( "\n$ATN$ERR\D{%F %H:%M}$RST" )
          else
            status1+=( "\n$HNT$NR\D{%F %H:%M}$RST" )
          fi
          status1+=( " $HNT$NR\u@\H [$(pwd)]$RST" )

          [[ "$todotxt_ps1" != "" ]] && status1+=( "$HNT$NR $todotxt_ps1$RST" )
          [[ "$git_ps1" != "" ]] && status1+=( "$HNT$NR$git_ps1$RST" )

          status2=()
          prompt="\s-\v\$"
          if [[ $result -eq 0 ]];
          then
            status2+=( "$HNT$OK$prompt$RST" )
          else
            status2+=( "$ATN$ERR($result) % $prompt$RST" )
          fi

          printf "%s" "''${status1[@]}\n''${status2[@]} "
        )
      '';
    in ''
      PROMPT_COMMAND='export PS1="$(. "${ps1}")"'
    '';
  };
}
