{ config, pkgs, lib, ... }:
with lib; {
  imports = [ ../modules/oculus.nix ];

  hardware = {
    vr.sidequest.enable = true;
    enableRedistributableFirmware = true;
  };

  nixpkgs.config.allowUnfree = true;
}
