{ config, lib, pkgs, ... }:
with lib;
let pipewire = config.services.pipewire;
in {
  # rtkit can offer better performance with pipewire
  # but I may need to add some additional configuration
  # at some point to make sure everything is working properly
  # see https://gitlab.freedesktop.org/pipewire/pipewire/-/wikis/Performance-tuning
  security.rtkit = mkIf pipewire.enable { enable = true; };

  services.pipewire = {
    enable = mkDefault true;
    alsa.enable = mkDefault true;
    alsa.support32Bit = mkIf pipewire.alsa.enable true;
    pulse.enable = true;
    jack.enable = true;
    socketActivation = true;
  };

  environment.systemPackages = mkIf pipewire.enable [
    pkgs.pavucontrol
    pkgs.pulseaudio-ctl
    pkgs.helvum
    pkgs.pulseaudio
  ];

  sound.enable = pipewire.alsa.enable;
  sound.mediaKeys.enable = mkDefault true;

}
