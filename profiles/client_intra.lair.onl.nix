{ config, lib, pkgs, ... }:

{
  services.openvpn.servers = {
    "intra.lair.onl" = {
      config = ''
        config "/nix/state/private/openvpn/vpn.intra.lair.onl.ovpn"
      '';
    };
  };
}
