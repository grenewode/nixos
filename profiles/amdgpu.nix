{ lib, config, pkgs, ... }:
with lib; {
  services.xserver.videoDrivers = [ "radeon" "amdgpu" ];

  hardware.opengl = mkIf (config.hardware.opengl.enable) {
    extraPackages = with pkgs; [
      rocm-opencl-icd
      rocm-opencl-runtime
      rocclr
      amdvlk
    ];
  };

  environment.systemPackages = with pkgs; [ radeontop radeon-profile rocm-smi ];
}
