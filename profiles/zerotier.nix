{ self, config, ... }: {
  services.zerotierone = {
    enable = true;
    joinNetworks = [ "e4da7455b2a3c8d8" ];
  };

  environment.state.directories = [ "/var/lib/zerotier-one" ];
}
