{ config, pkgs, lib, self, nixos-hardware, ... }:
with lib; {

  options.hardware.framework = {
    enable = lib.mkEnableOption "enable support for framework laptops";

    mainboard = {
      generation = lib.mkOption {
        description = "mainboard version";
        type = lib.types.enum [ "12th" "11th" ];
        default = "12th";
      };
    };

    intel12thGen =
      lib.mkEnableOption "enable support for intel 12th generation mainboards";
  };

  # imports =
  #   if config.hardware.framework.mainboard.generation == "12th" then [
  #     nixos-hardware.nixosModules.framework-12th-gen-intel
  #   ] else [
  #     nixos-hardware.nixosModules.framework
  #   ];

  config = lib.mkIf config.hardware.framework.enable {

    assertions = [{
      assertion = config.boot.kernelPackages.kernel.version != "5.19.12";
      message =
        "do not run kernel 5.19.12 on devices with intel GPUs: https://lore.kernel.org/all/YzwooNdMECzuI5+h@intel.com/";
    }];

    boot.initrd.availableKernelModules = [ "nvme" ]
      ++ (optionals config.services.hardware.bolt.enable [
        "thunderbolt"
        "thunderbolt-net"
      ]);

    hardware.bluetooth.enable = mkDefault true;
    hardware.bluetooth.powerOnBoot = mkDefault false;

    services.blueman.enable = mkDefault config.hardware.bluetooth.enable;

    services.hardware.bolt.enable = mkDefault true;

    services.uvcvideo.dynctrl = {
      enable = true;
      packages = with pkgs; [ tiscamera ];
    };

    environment.systemPackages = [ pkgs.powertop ];

    # https://community.frame.work/t/using-the-ax210-with-linux-on-the-framework-laptop/1844/93
    # boot.extraModprobeConfig = ''
    #   options iwlwifi disable_11ax=1
    #   options iwlwifi 11n_disable=1
    # '';

    services.fwupd.enable = true;

    services.thermald.enable = true;

    boot.extraModprobeConfig = ''
      options snd_hda_intel power_save=1
      options iwlwifi power_save=1
    '';

    boot.kernelParams =
      mkIf (config.hardware.framework.mainboard.generation == "12th")
      [ "i915.force_probe=46a6" ];

    services.tlp = {
      enable = true;
      settings = {

        # Setup CPU governor to save batter power
        CPU_SCALING_GOVERNOR_ON_AC = "performance";
        CPU_SCALING_GOVERNOR_ON_BAT = "powersave";
        CPU_ENERGY_PERF_POLICY_ON_AC = "performance";
        CPU_ENERGY_PERF_POLICY_ON_BAT = "power";

        CPU_MIN_PERF_ON_AC = 0;
        CPU_MAX_PERF_ON_AC = 100;
        CPU_MIN_PERF_ON_BAT = 0;
        CPU_MAX_PERF_ON_BAT = 30;
        PLATFORM_PROFILE_ON_AC = "performance";
        PLATFORM_PROFILE_ON_BAT = "low-power";

        PCIE_ASPM_ON_BAT = "powersupersave";
        NMI_WATCHDOG = "0";

        INTEL_GPU_MIN_FREQ_ON_AC = 100;
        INTEL_GPU_MIN_FREQ_ON_BAT = 100;
        INTEL_GPU_MAX_FREQ_ON_AC = 1300;
        INTEL_GPU_MAX_FREQ_ON_BAT = 450;
        INTEL_GPU_BOOST_FREQ_ON_AC = 1300;
        INTEL_GPU_BOOST_FREQ_ON_BAT = 450;
      };
    };
  };
}
