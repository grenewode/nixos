{ lib, pkgs, config, nixpkgs, self, ... }: {
  nix = {
    nixPath = [ "nixpkgs=${nixpkgs}" "self=${self}" ];

    registry = {
      nixpkgs = {
        from = {
          type = "indirect";
          id = "nixpkgs";
        };
        to = {
          type = "path";
          path = "${nixpkgs}";
        };
      };
      config = {
        from = {
          type = "indirect";
          id = "config";
        };
        to = {
          type = "path";
          path = "${self}";
        };
      };
    };

    package =
      lib.mkIf (lib.versionOlder pkgs.nix.version "2.4") pkgs.nixUnstable;
    extraOptions = ''
      experimental-features = nix-command flakes
      keep-outputs = true
      keep-derivations = true
    '';
  };
}
