{ pkgs, config, lib, self, ... }:
with lib; {

  imports = [ "${self}/profiles/sway-session.nix" ];

  programs.hyprland.enable = true;

  services.xserver = mkIf (!config.programs.sway.enable
    && !config.services.xserver.desktopManager.gnome.enable
    && !config.services.xserver.desktopManager.plasma5.enable) {
      enable = true;
      libinput.enable = true;

      windowManager.qtile.enable = true;

      displayManager = {
        lightdm.enable = true;

        defaultSession = "home-manager+qtile";

        session = [{
          manage = "desktop";
          name = "home-manager";

          start = "exec $HOME/.xsession";
        }];
      };
    };

  programs.steam.enable = mkDefault true;

  virtualisation.podman.enable = true;

  users.users.grenewode = {
    isNormalUser = true;
    createHome = true;

    extraGroups = ([ "video" "camera" "wheel" "dialout" "adbusers" ]
      ++ (lists.optional config.programs.wireshark.enable "wireshark")
      ++ (lists.optional config.programs.adb.enable "adbusers")
      ++ (lists.optional config.programs.steam.enable "game")
      ++ (lists.optional config.virtualisation.lxd.enable "lxd")
      # Allow me to control wifi connections
      ++ (with config.networking.wireless.userControlled;
        (lists.optional enable group))
      # Allows me to control virtual machines
      ++ (lists.optional
        (with config.virtualisation.libvirtd; enable && !qemu.runAsRoot)
        "libvirtd")
      # Allows me to control network manager
      ++ (lists.optional config.networking.networkmanager.enable
        "networkmanager"));

    autoSubUidGidRange = true;

    hashedPassword =
      "$6$0mrK7XZvfD$Waz/AgrLYrAbDBFmNVG.1yCu/uztUtqztSCcYcQI3urNRzSdgh8jqUNFQZymTvclIyPrAhXUy4evLs5mrUkIB0";
    shell = pkgs.zsh;

    openssh.authorizedKeys.keys = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDG+CTN3wwIiErQBwNDUUEx0VNromjEsDFp8N6y5x2U/nwOE05jC9NjKwf8MyM8f0mDqJfLAcfv2+tyQPP08ndtxSDwfCY0wFFcsVraksR84AULpCwJFuRJVV86O/S1Aat9n4iEMATdx/GSMWce1SnOpezanja/b43tliFN7OHfFsPgFKG+ojP9bh+bFu7B4xH8edFgMEbQAUHIqwb3xA00JW5l7h1wx/2QaGc+ucMwPgkxoubVE+O9Anio2Gwnu0nR4akBgEGXbwR5sUzV6DuiMAg/GRSHzeCiPc5NHEC7MOTPrIQh0x+j+triBebCw/ec95FRNlhXIMPiqoZhuGlIBTTm5uO18nZuSZ0cbL2pxDNBVk2ZBB3FNxiJ3JntzleNYk+K4EtFjvT2XBqwUNTvsuxJe6bvM0dvFvY9/tMjJJGpsCsxR9PWdUtHQO93JsQK1gld2lUA1c+JAdMMy13q4HfKe8yQGDw3D0qXYoAvy52RZlrXcwsOsHC1UqXZdaZaZ5xLtviLrtSTXwqg/3edq4uuuouezNzOUeY3JQ3wIOCahYs/wsQ+x9M2xWBzM6ZUfqtI8J0HwtRAm+9JfMkxYPEiLvoC8f8C4YZPgWg5JUlSu0f2/+i0dV4bddxPdjEGIdIp6j2DC8WK6CrR6Ve84FJr3UP4AF6vSq4MfyD54Q== cardno:11 467 797"
    ];
  };

  fileSystems."/home/grenewode" = mkDefault {
    device = "tmpfs";
    fsType = "tmpfs";

    options = [ "noatime" "nodev" "nosuid" ];
  };

  networking.firewall.allowedTCPPorts = [ 22000 ];
  networking.firewall.allowedUDPPorts = [ 22000 21027 ];

  home-manager.users.grenewode = { config, self, lib, nixosConfig, ... }: {
    home.state.directories = [ "Books" "Audiobooks" "Games" ".mozilla" ]
      ++ (lib.optionals
          (config.programs.neovim.enable || nixosConfig.programs.neovim.enable)
        [ "${config.xdg.configHome}/nvim" ])
      ++ (lib.optionals (nixosConfig.programs.steam.enable) ["${config.xdg.dataHome}/Steam"]);

    imports = [
      "${self}/hm-profiles/cad.nix"
      # "${self}/hm-profiles/dev/vscode.nix"
      "${self}/hm-profiles/gpg-agent-as-ssh-agent.nix"
      "${self}/hm-profiles/gpg-datum.nix"
      "${self}/hm-profiles/i3-datum"
      "${self}/hm-profiles/qtile.nix"
      "${self}/hm-profiles/shenv-datum.nix"
      "${self}/hm-profiles/ssh-datum.nix"
      "${self}/hm-profiles/sway-datum.nix"
      "${self}/hm-profiles/hyprland.nix"
      "${self}/hm-profiles/todotxt-datum.nix"
    ];

    virtualisation.containers.enable =
      nixosConfig.virtualisation.containers.enable;

    xdg.enable = true;

    xsession.windowManager.qtile.enable = true;
    xresources.extraConfig = mkDefault ''
      Xft.dpi: 192
    '';

    programs.foot.enable = true;
    programs.emacs.enable = true;

    programs.obs-studio.enable = true;

    programs.starship = {
      enable = true;
      settings = let
        fetchPreset = { url, hash }:
          let presetFile = (pkgs.fetchurl { inherit url hash; });
          in builtins.fromTOML (builtins.readFile presetFile);
        pastel-powerline = fetchPreset {
          url = "https://starship.rs/presets/toml/plain-text-symbols.toml";
          hash = "sha256-bxshLG17HfO6hVeufFMqAtl/UYjrEpFQZlmp3QHYwAo=";
        };

      in pkgs.lib.mkMerge [ pastel-powerline ];
    };
    programs.git = {
      enable = true;
      signing.key = "9A86 BC45 55A9 06C9 AFA3  DBDC 07F2 1430 CDBD E0AE";
    };
    services.gpg-agent.sshKeys = [ "9A86BC4555A906C9AFA3DBDC07F21430CDBDE0AE" ];

    # programs.minecraft.enable = mkDefault true;
    programs.prismlauncher = {
      enable = mkDefault true;
      package = pkgs.prismlauncher;
    };
    programs.calibre.enable = true;
    programs.vscode.enable = true;

    programs.blender.enable = true;
    programs.blender.package = pkgs.grenewode.blender;
    programs.firefox.enable = true;

    programs.password-store.enable = true;

    programs.godot.enable = true;

    programs.lutris.enable = true;
    programs.mindustry = { enable = true; };

    programs.wesnoth = { enable = true; };

    programs.discord.enable = true;
    programs.slack.enable = true;
    programs.intellij.enable = true;
    programs.lollypop = {
      enable = true;
      settings = {
        dark-ui = true;
        disable-scrobbling = false;
        force-single-column = false;
        hd-artwork = true;
        import-advanced-artist-tags = true;
        import-playlists = true;
        repeat = "auto_similar";
        shuffle = true;
      };
    };

    programs.ssh.enable = true;
    programs.gimp.enable = true;
    programs.inkscape.enable = true;
    programs.signal.enable = true;

    home.packages = (with pkgs; [
      vlc
      wl-clipboard
      xclip
      picard
      ranger
      # rstudio
      remmina
      xdg-utils
      asciinema
    ]) ++ (optionals nixosConfig.virtualisation.docker.enable
      [ pkgs.docker-compose ]) ++ (optionals nixosConfig.hardware.sane.enable [
        pkgs.xsane
        pkgs.gnome.simple-scan
      ]);

    home.sessionVariables = {
      EDITOR = "${pkgs.neovim}/bin/neovim";
      CARGO_REGISTRIES_CRATES_IO_PROTOCOL = "sparse";
    };

    programs.neovim.enable = true;
    programs.neovim.vimAlias = true;
    programs.neovim.viAlias = true;
    programs.neovim.vimdiffAlias = true;

    services.syncthing.enable = true;

    accounts.email.accounts."grenewode" = {
      address = "grenewode@pm.me";
      primary = true;
    };
  };
}
