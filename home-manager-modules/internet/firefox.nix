{ self, pkgs, config, lib, nixosConfig, ... }:
let inherit (lib) mkIf mkDefault;

in mkIf config.programs.firefox.enable {
  programs.firefox = {
    enableGnomeExtensions =
      mkDefault nixosConfig.services.xserver.desktopManager.gnome.enable;

    package = pkgs.firefox.override {
      cfg = {
        enableBrowserpass = true;
        pipewireSupport = nixosConfig.services.pipewire.enable;
      };
    };

    profiles = {
      default = {
        isDefault = true;

        extensions = with nixosConfig.nur.repos.rycee.firefox-addons; [
          privacy-badger
          browserpass
          clearurls
          multi-account-containers
          sidebery
          temporary-containers
          terms-of-service-didnt-read
          ublock-origin
          unpaywall
          search-by-image
          reduxdevtools
          react-devtools
          org-capture
          flagfox
          facebook-container
          decentraleyes
          stylus
          tampermonkey
        ];

        settings = {
          "xpinstall.signatures.required" = false;
          # Show more ssl cert infos
          "security.identityblock.show_extended_validation" = true;

          "widget.use-xdg-desktop-portal" = true;
          # "widget.content.allow-gtk-dark-theme"= true;
          # "widget.non-native-theme.enabled"= false;

          "gfx.webrender.enabled" = true;
          "gfx.webrender.compositor.force-enabled" = true;
          "gfx.webrender.all" = true;
          "gfx.webrender.precache-shaders" = true;

          # https://community.frame.work/t/linux-battery-life-tuning/6665/1
          # optimise GPU for intel graphics
          "media.rdd-ffmpeg.enabled" = true;
          "media.ffmpeg.vaapi.enabled" = true;
          "media.navigator.mediadatadecoder_vpx_enabled" = true;
          "media.ffvpx.enabled" = false;
          "media.rdd-vpx.enabled" = false;
          "media.rdd-process.enabled" = false;

          # "browser.tabs.drawInTitlebar"= true;
          "browser.uidensity" = 1;
          "alerts.showFavicons" = true;

          # "browser.display.use_system_colors"= true;
          "svg.context-properties.content.enabled" = true;
          "ui.prefersReducedMotion" = true;

          "browser.aboutConfig.showWarning" = false;
          "browser.tabs.warnOnCloseOtherTabs" = false;
          "browser.tabs.warnOnClose" = false;
          "browser.tabs.warnOnOpen" = false;
          "browser.warnOnQuit" = false;
          "browser.shell.checkDefaultBrowser" = false;
          "browser.aboutwelcome.enabled" = false;

          # Disable firefox accounts & syncing, since I don't use it
          "identity.fxaccounts.enabled" = false;

          # Disable pocket
          "extensions.pocket.enabled" = true;

          "pdfjs.disabled" = false;

          # New tabs & startup windows should always be blank
          "browser.startup.homepage" = "about:blank";
          "browser.startup.blankWindow" = true;
          "browser.newtabpage.enabled" = false;

          # Disable all autofill options
          "browser.formfill.enable" = false;
          "extensions.formautofill.addresses.enabled" = false;
          "extensions.formautofill.creditCards.enabled" = false;
          "extensions.formautofill.reauth.enabled" = false;
          "extensions.formautofill.section.enabled" = false;
          "signon.autofillForms" = false;
        };
      };
    };
  };

  # extraNativeMessagingHosts = [ browserpass ];

  # extraPolicies = {
  #   CaptivePortal = true;
  #   DisableFirefoxAccounts = true;
  #   DisableFirefoxStudies = true;
  #   DisableFormHistory = true;
  #   DisablePocket = true;
  #   DisableTelemetry = true;
  #   HardwareAcceleration = true;
  #   NewTabPage = false;
  #   NoDefaultBookmarks = true;
  #   OfferToSaveLogins = false;
  #   PasswordManagerEnabled = false;
  #   PictureInPicture = {
  #     Enabled = false;
  #     Locked = true;
  #   };

  #   PDFjs = {
  #     Enabled = true;
  #     EnablePermissions = false;
  #   };

  #   FirefoxHome = {
  #     Pocket = false;
  #     Snippets = false;
  #     Search = false;
  #     TopSites = false;
  #     Highlights = false;
  #     Locked = true;
  #   };

  #   Homepage = {
  #     URL = "about:blank";
  #     Locked = true;
  #     StartPage = "none";
  #   };

  #   UserMessaging = {
  #     ExtensionRecommendations = false;
  #     FeatureRecommendations = false;
  #     SkipOnboarding = true;
  #     WhatsNew = false;
  #   };

  #   EncryptedMediaExtensions = {
  #     Enabled = true;
  #     Locked = true;
  #   };

  #   ExtensionSettings = {
  #     "*" = {
  #       "blocked_install_message" =
  #         "You can't have manual extension mixed with nix extensions";
  #       "installation_mode" = "blocked";
  #     };
  #     "uBlock0@raymondhill.net" = {
  #       "installation_mode" = "force_installed";
  #       "install_url" = fetchFirefoxAddonFromAMO {
  #         name = "ublock-origin";
  #         hash = "sha256-vDwzXJYSactA3RFVF4jQ2GdK78rNyPvfbBmEXq6jOc4=";
  #       };
  #     };
  #     "browserpass@maximbaz.com" = {
  #       "installation_mode" = "force_installed";
  #       "install_url" = fetchFirefoxAddonFromAMO {
  #         name = "browserpass-ce";
  #         hash = "sha256-sXgUBbRvMnRpeIW1MTkmTcoqtW/8RDXAkxAq1evFkpc=";
  #       };
  #     };
  #     "jid1-MnnxcxisBPnSXQ@jetpack" = {
  #       "installation_mode" = "force_installed";
  #       "install_url" = fetchFirefoxAddonFromAMO {
  #         name = "privacy-badger17";
  #         hash = "sha256-UCdM0oBBO9DnxLU9LvPQGfajzhSnOW/tbSSPKVrn9j4=";
  #       };
  #     };
  #     "@contain-facebook" = {
  #       "installation_mode" = "force_installed";
  #       "install_url" = fetchFirefoxAddonFromAMO {
  #         name = "facebook-container";
  #         hash = "sha256-oYUfFa5Ox5DED5p1GtbWSkSmv0f3DuSX707hcRW7fgY=";
  #       };
  #     };
  #     "dont-track-me-google@robwu.nl" = {
  #       "installation_mode" = "force_installed";
  #       "install_url" = fetchFirefoxAddonFromAMO {
  #         name = "dont-track-me-google1";
  #         hash = "sha256-JZ7g/R6I7HyD6M8qhY39BJE974+OKCq8ZfDCIm7AAj4=";
  #       };
  #     };

  #     "{e4a8a97b-f2ed-450b-b12d-ee082ba24781}" = {
  #       "installation_mode" = "force_installed";
  #       "install_url" = fetchFirefoxAddonFromAMO {
  #         name = "greasemonkey";
  #         hash = "sha256-XrhalvdqmxakfPIHmR1CN79ZfHt2dXRVkgTi4P8Rc/A=";
  #       };
  #     };

  #     "{74145f27-f039-47ce-a470-a662b129930a}" = {
  #       "installation_mode" = "force_installed";
  #       "install_url" = fetchurl {
  #         url =
  #           "https://addons.mozilla.org/firefox/downloads/file/3927638/clearurls-1.24.1.xpi";
  #         hash = "sha256-QSWfxMMwJh4Z/004c061/a2S56NYoWF1OD4xxi1Q8Gs=";
  #       };
  #     };

  #     # "{3c078156-979c-498b-8990-85f7987dd929}" = {
  #     #   "installation_mode" = "force_installed";
  #     #   "install_url" = fetchFirefoxAddonFromAMO {
  #     #     name = "sidebery";
  #     #     hash = "sha256-9U2gM7pU7JcEJyEuwNLaufpc3FO6PwBSjA4TQPpIbGY=";
  #     #   };
  #     # };
  #   };
  # };

  # extraPrefs = "";
}
