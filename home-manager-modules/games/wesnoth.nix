{ config, lib, pkgs, ... }: {

  options.programs.wesnoth = {
    enable = lib.mkEnableOption
      "Enables The Battle for Wesnoth, an open source, turn-based strategy game with a high fantasy theme. It features both singleplayer and online/hotseat multiplayer combat.";

    package = lib.mkOption {
      default = pkgs.wesnoth;

      description = "package providing The Battle for Wesnoth";
    };
  };

  config = lib.mkIf config.programs.wesnoth.enable
    (with config.programs.wesnoth; {
      home.packages = [ package ];

      home.state.directories = with config.xdg; [
        "${configHome}/wesnoth"
        "${dataHome}/wesnoth/${lib.versions.majorMinor package.version}"
        "${cacheHome}/wesnoth"
      ];
    });
}
