{ pkgs, config, lib, options, ... }:
with lib; {
  options.programs.mindustry = {
    enable = mkEnableOption "enable Mindustry";
    package = mkOption {
      type = types.package;
      default = pkgs.mindustry;
    };
  };

  config.home.packages =
    (with config.programs.mindustry; lists.optional enable package);

  config.home.state.directories = (with config.programs.mindustry;
    lists.optional enable "${config.xdg.dataHome}/Mindustry");
}
