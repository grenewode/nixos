{ config, lib, pkgs, nixosConfig, ... }:
with lib; {
  options.programs.virt-manager = {
    enable = mkEnableOption
      "The virt-manager application is a desktop user interface for managing virtual machines through libvirt. It primarily targets KVM VMs, but also manages Xen and LXC (linux containers). It presents a summary view of running domains, their live performance & resource utilization statistics. Wizards enable the creation of new domains, and configuration & adjustment of a domain’s resource allocation & virtual hardware. An embedded VNC and SPICE client viewer presents a full graphical console to the guest domain.";
    package = mkOption {
      description = "the package to use to provide the virt-manager command";

      type = types.package;
      default = pkgs.virt-manager;
    };

    connections = mkOption {
      description =
        "the default connections that should be enabled for virt-manager.";
      default = { };
      type = with types;
        (attrsOf (submodule {
          options.autoconnect =
            mkEnableOption "Should virt-manager autoconnect to this uri?";
        }));
    };
  };

  config.home.packages = with lib.lists;
    with config.programs.virt-manager;
    (optionals enable ([ package pkgs.virt-top pkgs.virt-viewer ]
      ++ (optionals nixosConfig.virtualisation.lxc.enable [
        pkgs.pipework
        pkgs.distrobuilder
      ])));

  config.dconf.settings."org/virt-manager/virt-manager/connections" =
    mkIf config.programs.virt-manager.enable (rec {
      autoconnect = builtins.filter
        (uri: config.programs.virt-manager.connections."${uri}".autoconnect)
        uris;
      uris = builtins.attrNames config.programs.virt-manager.connections;
    });
}
