{ self, config, pkgs, lib, ... }:
let
  inherit (lib) mkIf mkDefault;
in mkIf config.programs.emacs.enable {

  home = {
    sessionPath = [ "${config.xdg.configHome}/emacs/bin" ];
    sessionVariables = {
      DOOMDIR = "${config.xdg.configHome}/doom";
      DOOMLOCALDIR = "${config.xdg.dataHome}/doom";
    };
  };

  home.state.directories = [
    config.home.sessionVariables.DOOMDIR
    config.home.sessionVariables.DOOMLOCALDIR
    "${config.xdg.configHome}/emacs"
    "org"
    ".zotero"
  ];

  programs.emacs = {
    package = mkDefault pkgs.emacs29-pgtk;
    extraPackages = epkgs: with epkgs; [ vterm pdf-tools tree-sitter ];
  };

  services.emacs = {
    enable = mkDefault true;
    client.enable = true;
    client.arguments = [ "-c" ];
    socketActivation.enable = true;
  };

  home.packages = with pkgs; [
    zotero
    sqlite
    texlive.combined.scheme-medium
    emacs-all-the-icons-fonts
    nixfmt
    hunspell
    hunspellDicts.en_US
    (nerdfonts.override { fonts = [ "RobotoMono" ]; })
  ];
}
