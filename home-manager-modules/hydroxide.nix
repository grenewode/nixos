{ config, pkgs, lib, ... }:
with lib; {
  options.services.hydroxide = {
    enable =
      mkEnableOption "enable the hydroxide open-source ProtonMail bridge";

    package = mkOption {
      description =
        "Package providing the hydroxide executable that will be used to start the service";
      type = types.package;
      default = pkgs.hydroxide;
    };

    smtp = {
      enable = mkEnableOption "Enable SMTP for hydroxide serve";
      host = mkOption {
        description = "Allowed SMTP email hostname on which hydroxide listens";
        type = types.str;
        default = "127.0.0.1";
      };
      port = mkOption {
        description = "SMTP port on which hydroxide listens";
        type = types.unsigned;
        default = 1025;
      };
    };
    imap = {
      enable = mkEnableOption "Enable IMAP for hydroxide serve";
      host = mkOption {
        description = "Allowed IMAP email hostname on which hydroxide listens";
        type = types.str;
        default = "127.0.0.1";
      };
      port = mkOption {
        description = "IMAP port on which hydroxide listens";
        type = types.unsigned;
        default = 1143;
      };
    };
    carddav = {
      enable = mkEnableOption "Enable CardDAV for hydroxide serve";
      host = mkOption {
        description = "Allowed CardDAV hostname on which hydroxide listens";
        type = types.str;
        default = "127.0.0.1";
      };
      port = mkOption {
        description = "CardDAV port on which hydroxide listens";
        type = types.unsigned;
        default = 1143;
      };
    };
  };

  config.systemd.user.services.hydroxide =
    mkIf (config.services.hydroxide.enable) {
      Unit.Description =
        "Launch the hydroxide ProtonMail bridge when the user logs in";

      Service = { };
    };
}

