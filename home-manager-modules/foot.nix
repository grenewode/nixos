{ config, lib, pkgs, ... }:
let inherit (lib) mkIf;
in {
  programs.foot = mkIf config.programs.foot.enable {
    settings = {
      main = {
        term = "xterm-256color";

        font = "monospace:size=14";
        dpi-aware = "yes";
      };

      mouse = { hide-when-typing = "yes"; };
    };
  };
}
