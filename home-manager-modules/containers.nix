{ pkgs, config, lib, ... }:
let
  inherit (lib) mkIf mkEnableOption mkOption removePrefix;
  inherit (config) xdg;
  toml = pkgs.formats.toml { };

  containers = config.virtualisation.containers;

in {
  options.virtualisation.containers = {
    enable = mkEnableOption
      "This option enables the common ${xdg.configHome}/containers configuration module.";

    storage.settings = mkOption {
      description = "storage.conf configuration";
      type = toml.type;
      default = {
        storage = {
          driver = "overlay";
          graphroot = "${xdg.dataHome}/storage";
          runroot = "$XDG_RUNTIME_DIR/storage";
        };
      };
    };
  };

  config = mkIf containers.enable {

    xdg.configFile."containers/storage.conf" = {
      source = toml.generate "storage.conf" containers.storage.settings;
    };
    
    home.state.directories = [ (removePrefix config.home.homeDirectory containers.storage.settings.storage.graphroot) ];
  };
}
