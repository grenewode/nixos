{ config, lib, pkgs, options, ... }:
with lib; {
  options.programs.godot = {
    enable = mkEnableOption "enable the Godot game engine";
    package = mkOption {
      type = types.package;
      default = pkgs.godot;
    };
  };

  config.home = mkIf config.programs.godot.enable (with config.programs.godot; {
    packages = [ package ];

    state.directories = [
      "${config.xdg.configHome}/godot/"
      "${config.xdg.dataHome}/godot/"
      "${config.xdg.cacheHome}/godot/"
    ];
  });
}
