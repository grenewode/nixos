{ config, lib, pkgs, options, ... }:
with lib; {
  options.programs.inkscape = {
    enable = mkEnableOption config.programs.inkscape.package.description;
    package = mkOption {
      type = types.package;
      default = pkgs.inkscape;
    };
  };

  config.home = with config.programs.inkscape;
    mkIf enable {
      packages = [ package ];

      state.directories = let inherit (config.xdg) configHome;
      in [ "${configHome}/inkscape" ];
    };
}
