{ config, lib, pkgs, options, ... }:
with lib; {
  options.programs.gimp = {
    enable = mkEnableOption config.programs.gimp.package.description;
    package = mkOption {
      type = types.package;
      default = pkgs.gimp;
    };
  };

  config.home = with config.programs.gimp;
    mkIf enable {
      packages = [ package ];

      state.directories = let
        inherit (config.xdg) configHome;
        enableXdg = lib.versionAtLeast package.version "2.10";
      in if enableXdg then
        [ "${configHome}/GIMP/2.10" ]
      else
        [ ".gimp-${lib.versions.majorMinor package.version}" ];
    };
}
