{ config, lib, pkgs, options, ... }:
with lib; {
  options.programs.signal = {
    enable = mkEnableOption config.programs.signal.package.description;
    package = mkOption {
      type = types.package;
      default = pkgs.signal-desktop;
    };
  };

  config.home = with config.programs.signal;
    mkIf enable {
      packages = [ package ];

      state.directories = let inherit (config.xdg) configHome;
      in [ "${configHome}/Signal" ];
    };
}
