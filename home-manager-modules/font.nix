{ config, lib, pkgs, ... }:

with lib;

let
  fontModule = types.submodule ({ ... }: {
    package = mkOption {
      description =
        "The package providing the font. If this value is null, then the font is assumed to already be installed on your system. For example, if the font is provided by a font pack.";
      type = types.nullOr types.package;

      default = null;
    };
    name = mkOption {
      description = "The family name within the given package";
      type = types.string;
    };

    size = mkOption {
      description = "The size of the font";
      type = types.nullOr types.ints.unsigned;
      default = null;
    };
  });
in {
  options.fonts = {
    defaults = {
      ui = mkOption {
        description =
          "The font to use for text in most application user interfaces.";
        type = with types; nullOr fontModule;
        default = null;
      };
      monospace = mkOption {
        description =
          "The font to use for monospace (code or other fixed-width) text.";
        type = with types; nullOr fontModule;
        default = null;
      };

      document = mkOption {
        description = "The font to use for document (prose) text.";
        type = with types; nullOr fontModule;
        default = null;
      };
    };
  };

  config = let fonts = config.fonts.defaults;
  in {
    gtk.font = mkIf (config.gtk.enable && fonts.ui != null) ({
      package = mkDefault fonts.ui.package;
      name = mkDefault fonts.ui.name;
      size = mkDefault fonts.ui.size;
    });

    xdg.configFile."fontconfig/conf.d/50-user" =
      mkIf (config.fonts.fontconfig.enable) {
        text = ''
          <?xml version="1.0"?>
          <!DOCTYPE fontconfig SYSTEM "urn:fontconfig:fonts.dtd">
          <fontconfig>
          <alias>
              <family>monospace</family>
              <prefer>
                  <family>Bitstream Vera Sans Mono</family>
              </prefer>
          </alias>
          </fontconfig>
        '';
      };
  };
}
