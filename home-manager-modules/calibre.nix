{ config, lib, pkgs, ... }:
with lib; {
  options.programs.calibre = {
    enable = mkEnableOption "enable calibre";
    package = mkOption {
      type = types.package;
      default = pkgs.calibre;
    };

    configDir = mkOption {
      type = types.path;
      default = "${config.xdg.configHome}/calibre";
    };

    cacheDir = mkOption {
      type = types.path;
      default = "${config.xdg.cacheHome}/calibre";
    };

    useSystemTheme = mkOption {
      type = types.bool;
      default = true;
    };
  };

  config = mkIf config.programs.calibre.enable (with config.programs; {
    home.packages = [ calibre.package ];
    home.sessionVariables = {
      CALIBRE_CONFIG_DIRECTORY = calibre.configDir;
      CALIBRE_CACHE_DIRECTORY = calibre.cacheDir;
      CALIBRE_USE_SYSTEM_THEME = if calibre.useSystemTheme then 1 else 0;
    };

    home.state.directories = [ calibre.configDir ];
  });
}
