{ pkgs, config, lib, options, ... }:
with lib; {
  options.programs.minecraft = {
    enable = mkEnableOption "enable minecraft";
    package = mkOption {
      type = types.package;
      default = pkgs.unstable.minecraft;
    };
  };

  options.programs.prismlauncher = {
    enable = mkEnableOption "Enable the PrismLauncher minecraft launcher";
    package = mkOption {
      type = types.package;
      default = pkgs.prismlauncher-qt5;
    };
  };

  options.programs.polymc = options.programs.prismlauncher;

  config.home.packages =
    (with config.programs.minecraft; lists.optional enable package)
    ++ (with config.programs.prismlauncher; lists.optional enable package);

  config.home.state.directories =
    (with config.programs.minecraft; lists.optional enable ".minecraft")
    ++ (with config.programs.prismlauncher;
      lists.optional enable "${config.xdg.dataHome}/PrismLauncher");
}
