{ config, lib, pkgs, options, ... }:
with lib; {
  options.programs.lutris = {
    enable = mkEnableOption "enable the lutris game launcher";
    package = mkOption {
      type = with types; package;
      default = pkgs.lutris;
    };
  };

  config.home = mkIf config.programs.lutris.enable (with config.programs; {
    packages = [ lutris.package ];

    state.directories = [
      "${config.xdg.cacheHome}/lutris"
      "${config.xdg.configHome}/lutris"
      "${config.xdg.dataHome}/lutris"
    ];
  });
}

