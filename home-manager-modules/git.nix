{ config, lib, pkgs, ... }:
let
  inherit (lib) head filter mkIf mkDefault;
  inherit (builtins) attrValues;
  inherit (config.accounts) email;
  inherit (pkgs) git-ignore gh gitFull;

  primaryAccount =
    head (filter (account: account.primary) (attrValues email.accounts));

in mkIf config.programs.git.enable {

  home.packages = [ git-ignore gh ];

  programs.git = {
    package = mkDefault gitFull;

    lfs = { enable = true; };

    userEmail = primaryAccount.address;
    userName = primaryAccount.name;

    extraConfig.core.editor = config.home.sessionVariables.EDITOR or "nano";

    aliases.graph =
      "log --graph --pretty=oneline  --decorate --all --abbrev-commit";

    signing = { signByDefault = true; };
  };
}
