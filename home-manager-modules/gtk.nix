{ config, lib, pkgs, ... }:
let
  inherit (config) gtk;
  inherit (lib) mkDefault mkIf versionAtLeast;
  stateVersionAtLeast = minVersion:
    versionAtLeast config.home.stateVersion minVersion;
  stateVersionBefore = maxVersion: !(stateVersionAtLeast maxVersion);
in {
  config = mkIf gtk.enable {
    gtk = {
      gtk2 = {
        configLocation = mkDefault "${config.xdg.configHome}/gtk-2.0/gtkrc";
      };

      gtk3 = {
        bookmarks = map (dir: "file://${dir}") (with config.xdg.userDirs; [
          desktop
          documents
          pictures
          videos
          download
          publicShare
          templates
        ]);

        extraConfig = {
          gtk-cursor-theme-size = if stateVersionAtLeast "22.05" then
            config.home.pointerCursor.size
          else
            config.xsession.pointerCursor.size;
        };
      };

      font = mkDefault {
        package = pkgs.roboto;
        name = "Roboto";
        size = 12;
      };

      theme = mkDefault {
        package = pkgs.plano-theme;
        name = "Plano";
      };

      iconTheme = mkDefault {
        package = pkgs.paper-icon-theme;
        name = "Paper";
      };
    };

    # qt = {
    #   platformTheme = "gnome";
    #   style = {
    #     package = pkgs.arc-theme;
    #     name = "adwaita-dark";
    #   };
    # };

    xsession.pointerCursor =
      mkIf (lib.debug.traceVal (stateVersionBefore "22.05")) {
        package = config.gtk.iconTheme.package;
        name = config.gtk.iconTheme.name;
      };

    home.pointerCursor = mkIf (stateVersionAtLeast "22.05") {
      package = config.gtk.iconTheme.package;
      name = config.gtk.iconTheme.name;

      gtk = { enable = true; };
    };
  };
}
