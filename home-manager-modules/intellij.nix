{ self, config, nixosConfig, pkgs, lib, ... }:
with lib; {
  options.programs.intellij = {
    enable = mkEnableOption
      "enables the JetBrains IntelliJ IDEA IDE for software development";
    package = mkOption {
      description =
        "which package provides the IntelliJ IDEA - generally, this is used to choose between the free and nonfree versions of the IDE.";
      type = types.package;
      default = if nixosConfig.system.stateVersion <= "21.11" then
        pkgs.idea.idea-ultimate
      else
        pkgs.jetbrains.idea-ultimate;
    };
  };

  config = mkIf config.programs.intellij.enable {
    home.packages = with pkgs; [ config.programs.intellij.package ];

    home.state.directories = [
      ".java/"
      (strings.removePrefix config.home.homeDirectory
        "${config.xdg.configHome}/JetBrains")
      (strings.removePrefix config.home.homeDirectory
        "${config.xdg.dataHome}/JetBrains")
      (strings.removePrefix config.home.homeDirectory
        "${config.xdg.cacheHome}/JetBrains")
    ];
  };
}
