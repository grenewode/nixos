{ config, pkgs, lib, options, ... }:
with lib; {
  options.programs.slack = {
    enable = mkEnableOption "enable the slack chat application";
    package = mkOption {
      type = types.package;
      default = pkgs.slack;
    };
  };

  config.home = mkIf config.programs.slack.enable {
    packages = [ config.programs.slack.package ];
    state.directories =
      mkIf (options.home ? state) [ "${config.xdg.configHome}/Slack" ];
  };
}
