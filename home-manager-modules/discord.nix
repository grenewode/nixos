{ config, pkgs, lib, ... }:
with lib; {
  options.programs.discord = {
    enable = mkEnableOption "enable the discord chat application";
    package = mkOption {
      type = types.package;
      default = pkgs.discord;
    };
  };

  config.home = mkIf config.programs.discord.enable {
    packages = [ config.programs.discord.package ];
    state.directories = [ "${config.xdg.configHome}/discord" ];
  };

}
