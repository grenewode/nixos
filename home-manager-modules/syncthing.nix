{ config, lib, options, ... }:
let inherit (lib) mkIf;
in {
  config = mkIf config.services.syncthing.enable {
    home.state.directories = [ "${config.xdg.configHome}/syncthing" ];
  };
}
