{ pkgs, config, lib, nixosConfig, self, ... }:
with lib;
let
  normalize = removePrefix config.home.homeDirectory;
  cfg = config.home.state;
in {
  options.home.state = {
    store = mkOption { type = types.str; };

    allowOther = mkOption { type = types.bool; };

    files = with types;
      mkOption {
        type = listOf str;
        default = [ ];
        description =
          "a list of files that should be stateful (ie, persist between boots)";
      };

    directories = with types;
      mkOption {
        type = listOf str;
        default = [ ];
        description =
          "a list of directories that should be stateful (ie, persist between boots)";
      };
  };

  config.home.state.store = mkDefault
    "${nixosConfig.environment.state.store}${config.home.homeDirectory}";
  config.home.state.directories =
    mkDefault [ "${config.xdg.configHome}/dconf" ];

  config.home.state.allowOther =
    mkDefault nixosConfig.programs.fuse.userAllowOther;

  config.home.persistence.${config.home.state.store} =
    mkIf (cfg.files != [ ] || cfg.directories != [ ]) {
      inherit (cfg) allowOther;
      files = map normalize cfg.files;
      directories = map normalize cfg.directories;
    };
}
