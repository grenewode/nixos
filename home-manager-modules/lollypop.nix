{ config, lib, pkgs, options, ... }:
with lib; {
  options.programs.lollypop = {
    enable = mkEnableOption
      "Lollypop is a lightweight modern music player designed to work excellently on the GNOME desktop environment.";
    package = mkOption {
      type = types.package;
      default = pkgs.lollypop;
    };

    settings = mkOption {
      description = "Settings for lollypop that will be written to dconf";
      type = types.attrsOf hm.types.gvariant;
    };

  };

  config = mkIf config.programs.lollypop.enable
    (with config.programs.lollypop; {
      home.packages = [ package ];

      programs.lollypop.settings.music-uris = mkDefault
        (hm.gvariant.mkArray hm.gvariant.type.string
          [ "file://${config.xdg.userDirs.music}" ]);

      dconf.settings = { "org/gnome/Lollypop" = settings; };

      home.state.directories = [
        "${config.xdg.dataHome}/lollypop"
        "${config.xdg.cacheHome}/lollypop"
      ];
    });
}
