{ lib, config, pkgs, ... }: {
  programs.obs-studio = lib.mkIf config.programs.obs-studio.enable {
    plugins = with pkgs.obs-studio-plugins;
      ((lib.optionals config.wayland.windowManager.sway.enable [ wlrobs ])
        ++ [ obs-ptz ]);
  };
}
