{ config, lib, pkgs, options, ... }:
with lib; {
  options.programs.blender = let
    version = builtins.splitVersion config.programs.blender.package.version;
    major = lists.elemAt version 0;
    minor = lists.elemAt version 1;
  in {
    enable = mkEnableOption "enable the blender 3d graphics program";
    package = mkOption {
      type = types.package;
      default = pkgs.blender;
    };

    userConfig = mkOption {
      type = types.path;

      default = "${config.xdg.configHome}/blender/${major}.${minor}/config";
    };

    userScripts = mkOption {
      type = types.path;

      default = "${config.xdg.configHome}/blender/${major}.${minor}/scripts";
    };

    userDatafiles = mkOption {
      type = types.path;

      default = "${config.xdg.dataHome}/blender/${major}.${minor}";
    };
  };

  config.home = mkIf config.programs.blender.enable
    (with config.programs.blender; {
      packages = [ package ];

      sessionVariables = {
        BLENDER_USER_CONFIG = userConfig;
        BLENDER_USER_SCRIPTS = userScripts;
        BLENDER_USER_DATAFILES = userDatafiles;
      };

      state.directories =
        mkIf (options.home ? state) [ userConfig userScripts userDatafiles ];
    });
}
