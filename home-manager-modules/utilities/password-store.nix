{ config, lib, pkgs, options, ... }:
with lib;
let
  cfg = config.programs.password-store;
  firefox = config.programs.firefox;
  chromium = config.programs.chromium;
  rofi = config.programs.rofi;
in {
  config = mkIf cfg.enable {
    home.packages = [ pkgs.mkpasswd ];

    home.state.directories = [ cfg.settings.PASSWORD_STORE_DIR ];

    programs = {
      password-store = {
        package = pkgs.pass.withExtensions
          (exts: [ exts.pass-genphrase exts.pass-otp ]);

        settings = {
          PASSWORD_STORE_DIR =
            mkDefault "${config.xdg.dataHome}/password-store";
        };
      };

      browserpass = mkIf (firefox.enable || chromium.enable) {
        enable = true;

        browsers = (optional firefox.enable "firefox")
          ++ (optional chromium.enable "chromium");
      };

      rofi.pass = mkIf rofi.enable {
        enable = true;
        stores = [ cfg.settings.PASSWORD_STORE_DIR ];
      };
    };
  };
}
