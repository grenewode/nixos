{ config, pkgs, lib, modulesPath, self, nixos-hardware, ... }:
with lib; {

  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
    "${self}/users/grenewode"
    "${self}/profiles/framework-laptop.nix"
    nixos-hardware.nixosModules.framework-12th-gen-intel
    # "${self}/profiles/amdgpu.nix"
    "${self}/profiles/workstation.nix"
  ];

  nixpkgs.system = "x86_64-linux";
  nixpkgs.localSystem = { config = "x86_64-unknown-linux-gnu"; };

  networking = {
    hostName = "mercury";
    domain = "intra.lair.onl";

    useDHCP = false;
    networkmanager = { enable = true; };

    # Prevent the laptop boot up from hanging when we are not connected to WiFi or Ethernet
    dhcpcd = mkIf (!config.networking.networkmanager.enable
      && !config.networking.useNetworkd) { wait = "background"; };

    wireless = mkIf (!config.networking.networkmanager.enable) {
      enable = true;
      scanOnLowSignal = false;
      userControlled = {
        enable = true;
        group = "network";
      };
    };

    useNetworkd = mkDefault (!config.networking.networkmanager.enable);

    interfaces = mkIf (!config.networking.networkmanager.enable) {
      wlp170s0.useDHCP = true;
      enp0s13f0u1u4u5.useDHCP = true;
    };
  };

  systemd.network.enable = mkDefault config.networking.useNetworkd;

  users.extraGroups = listToAttrs
    ((with config.networking.wireless.userControlled;
      (optional enable {
        name = group;
        value = { };
      })));

  environment.state = {
    directories = [
      "/var/log"
      "/var/lib/bluetooth"
      "/etc/NetworkManager/system-connections"
    ];
    files = (optional config.networking.wireless.userControlled.enable
      "/etc/wpa_supplicant.conf");
  };

  boot = {

    kernelParams = [ "boot.shell_on_fail" ];

    loader = {
      efi.efiSysMountPoint = "/boot";
      efi.canTouchEfiVariables = true;

      grub = mkDefault {
        enable = true;
        device = "nodev";
        efiSupport = true;
        enableCryptodisk = true;
        fsIdentifier = "provided";
      };
    };

    initrd = { availableKernelModules = [ "nvme" "thunderbolt" ]; };
  };

  security.rtkit.enable = true;

  fileSystems = {
    "/boot" = {
      device = "/dev/disk/by-label/BOOT";
      fsType = "vfat";
      neededForBoot = true;
    };

    "/" = mkDefault {
      device = "tmpfs";
      fsType = "tmpfs";

      options = [ "defaults" "noatime" "nosuid" "size=1G" ];
    };

    "/tmp" = mkDefault {
      device = "tmpfs";
      fsType = "tmpfs";

      options = [ "defaults" "noatime" "nodev" "nosuid" ];
    };

    "/nix" = {
      device = "/dev/mapper/PLAIN_DATA";
      fsType = "btrfs";
      options = [ "compress=zstd" "noatime" ];
      neededForBoot = true;

      encrypted = {
        enable = true;
        label = "PLAIN_DATA";
        blkDev = "/dev/disk/by-uuid/c72bb4f7-f85a-4c42-a427-9df58e3eea58";
      };
    };
  };

  swapDevices = [{
    device = "/dev/mapper/PLAIN_SWAP";

    encrypted = {
      enable = true;
      label = "PLAIN_SWAP";
      blkDev = "/dev/disk/by-label/CRYPT_SWAP";
    };
  }];

  # TODO: check if we are using pipewire
  hardware.pulseaudio.enable = mkForce false;

  hardware.framework.enable = true;

  # hardware.nvidia = {
  #   modesetting.enable = true;
  #   nvidiaSettings = true;

  #   prime = {
  #     offload.enable = true;
  #     # sync.enable = true;
  #     intelBusId = "PCI:0:2:0";
  #     amdgpuBusId = "PCI:06:00:0";
  #   };
  # };

  services.avahi = {
    enable = true;
    publish.enable = true;
    nssmdns = true;
  };

  virtualisation.containers.storage.settings = {
    storage = {
      driver = "btrfs";
      graphroot = "/var/lib/containers/storage";
      runroot = "/run/containers/storage";
    };
  };

  home-manager.sharedModules = [
    ({ config, lib, nixosConfig, ... }: {
      virtualisation.containers.storage.settings.storage = {
        driver = nixosConfig.virtualisation.containers.storage.settings.storage.driver;
        graphroot = config.home.state.store + (lib.removePrefix config.home.homeDirectory "${config.xdg.dataHome}/containers/storage");
        runroot = "$XDG_RUNTIME_DIR/storage";
      };
    })
  ];

  # services.xserver.desktopManager.xfce = {
  #   enable = true;
  # };

  # programs.thunar.enable = mkDefault config.services.xserver.desktopManager.xfce.enable;

  services.xserver.videoDrivers = [ ];
}
