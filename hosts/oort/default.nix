{
  system = "x86_64-linux";

  configurations = [
    ({ self, modulesPath, packages, config, lib, pkgs, nixos-hardware, ... }:
      with lib; {
        imports = [
          (modulesPath + "/installer/scan/not-detected.nix")
          "${self}/users/grenewode"
          "${self}/profiles/personal-computer"
          "${self}/profiles/intelgpu.nix"
          "${nixos-hardware}/asus/battery.nix"
          "${nixos-hardware}/common/cpu/intel"
          "${nixos-hardware}/common/pc/laptop"
        ];

        environment.state = {
          directories = [
            "/var/log"
            "/var/lib/bluetooth"
            "/etc/NetworkManager/system-connections"
          ];
          files = (optional config.networking.wireless.userControlled.enable
            "/etc/wpa_supplicant.conf");
        };

        networking = {
          hostName = "oort";
          useDHCP = false;

          # Prevent the laptop boot up from hanging when we are not connected to WiFi or Ethernet
          dhcpcd.wait = "background";

          wireless = mkIf (!config.networking.networkmanager.enable) {
            enable = true;
            userControlled = {
              enable = true;
              group = "network";
            };
          };
          networkmanager.enable = true;
        };

        users.extraGroups = listToAttrs
          ((with config.networking.wireless.userControlled;
            (optional enable {
              name = group;
              value = { };
            })));

        programs.steam.enable = false;
        hardware.bluetooth.enable = mkDefault true;
        hardware.bluetooth.powerOnBoot = mkDefault false;
        services.blueman.enable = mkDefault config.hardware.bluetooth.enable;

        home-manager.users.grenewode = {
          programs.minecraft.enable = false;
          programs.blender.enable = false;
          programs.intellij.enable = false;
        };

        services.thermald.enable = true;
        services.tlp = {
          enable = true;
          settings = {

            # Setup CPU governor to save batter power
            CPU_SCALING_GOVERNOR_ON_AC = "performance";
            CPU_SCALING_GOVERNOR_ON_BAT = "powersave";
            CPU_ENERGY_PERF_POLICY_ON_AC = "performance";
            CPU_ENERGY_PERF_POLICY_ON_BAT = "power";

            CPU_MIN_PERF_ON_AC = 0;
            CPU_MAX_PERF_ON_AC = 100;
            CPU_MIN_PERF_ON_BAT = 0;
            CPU_MAX_PERF_ON_BAT = 30;
            PLATFORM_PROFILE_ON_AC = "performance";
            PLATFORM_PROFILE_ON_BAT = "low-power";
          };
        };

        boot.loader = {
          efi.efiSysMountPoint = "/boot";
          efi.canTouchEfiVariables = true;

          grub = mkDefault {
            enable = true;
            device = "nodev";
            efiSupport = true;
            enableCryptodisk = true;
            fsIdentifier = "provided";
          };

        };

        boot.initrd.luks = {
          devices."PLAIN_DATA" = { device = "/dev/disk/by-label/CRYPT_DATA"; };
        };

        security.rtkit.enable = true;
        services.avahi = {
          enable = true;
          publish.enable = true;
          nssmdns = true;
        };

        fileSystems = {
          "/boot" = {
            device = "/dev/disk/by-label/BOOT";
            fsType = "vfat";
            neededForBoot = true;
          };

          "/" = mkDefault {
            device = "tmpfs";
            fsType = "tmpfs";

            options = [ "defaults" "noatime" "nosuid" "size=512m" "mode=0755" ];
          };

          "/tmp" = {
            device = "/dev/mapper/PLAIN_DATA";
            fsType = "btrfs";
            options = [ "subvol=@/tmp" "compress=zstd" "noatime" "mode=1777" ];
            neededForBoot = true;
          };

          "/nix/store" = {
            device = "/dev/mapper/PLAIN_DATA";
            fsType = "btrfs";
            options = [ "compress=zstd" "noatime" "subvol=@/nix/store" ];
            neededForBoot = true;
          };

          "/nix/state" = {
            device = "/dev/mapper/PLAIN_DATA";
            fsType = "btrfs";
            options = [ "compress=zstd" "noatime" "subvol=@/nix/state" ];
            neededForBoot = true;
          };
        };
      })
  ];
}
