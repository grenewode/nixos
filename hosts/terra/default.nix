{ config, pkgs, lib, self, modulesPath, ... }:
with lib; {
  nixpkgs.system = "x86_64-linux";
  nixpkgs.localSystem = { config = "x86_64-unknown-linux-gnu"; };

  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
    "${self}/users/grenewode"
    "${self}/profiles/workstation.nix"

    "${self}/profiles/amdcpu.nix"
    "${self}/profiles/amdgpu.nix"
    "${self}/profiles/nvidiagpu.nix"
    # ./fileSystems.nix
  ];

  system.nixos.tags = [ "desktop" ];

  networking.hostName = "terra";
  networking.interfaces.enp5s0.useDHCP = true;
  networking.interfaces.wlp4s0.useDHCP = true;

  environment.state = {
    store = "/state";
    directories = [
      "/var/log"
      "/var/lib/bluetooth"
      "/etc/NetworkManager/system-connections"
    ];
    files = (optional config.networking.wireless.userControlled.enable
      "/etc/wpa_supplicant.conf");
  };

  # environment.systemPackages = [ spacenavd ];

  # Since this is a desktop computer, it doesn't really make sense to have these enabled at all.
  systemd.targets.sleep.enable = false;
  systemd.targets.suspend.enable = false;
  systemd.targets.hibernate.enable = false;
  systemd.targets.hybrid-sleep.enable = false;

  boot = {
    initrd = {
      availableKernelModules = [ "nvme" "xhci_pci" "ahci" "usbhid" "sd_mod" ];
    };

    loader = {
      timeout = 5;
      efi.efiSysMountPoint = "/boot";
      efi.canTouchEfiVariables = true;

      grub = mkDefault {
        enable = true;
        device = "nodev";
        efiSupport = true;
        enableCryptodisk = true;
        fsIdentifier = "provided";
      };
    };
  };

  security.rtkit.enable = true;

  fileSystems = {
    "/boot" = {
      device = "/dev/disk/by-label/BOOT";
      fsType = "vfat";
      neededForBoot = true;
    };

    "/" = mkDefault {
      device = "tmpfs";
      fsType = "tmpfs";

      options = [ "defaults" "noatime" "nosuid" "size=1G" ];
    };

    "/tmp" = mkDefault {
      device = "tmpfs";
      fsType = "tmpfs";

      options = [ "defaults" "noatime" "nodev" "nosuid" ];
    };

    "/nix" = {
      device = "/dev/mapper/PLAIN_DATA";
      fsType = "btrfs";
      options = [ "subvol=@/nix" "compress=zstd" "noatime" ];
      neededForBoot = true;

      encrypted = {
        enable = true;
        label = "PLAIN_DATA";
        blkDev = "/dev/disk/by-label/CRYPT_DATA";
      };
    };

    "/state" = {
      device = "/dev/mapper/PLAIN_DATA";
      fsType = "btrfs";
      options = [ "subvol=@/state" "compress=zstd" "noatime" ];
      neededForBoot = true;

      encrypted = {
        enable = true;
        label = "PLAIN_DATA";
        blkDev = "/dev/disk/by-label/CRYPT_DATA";
      };
    };

    "/.snapshots" = {
      device = "/dev/mapper/PLAIN_DATA";
      fsType = "btrfs";
      options = [ "subvol=/.snapshots" "compress=zstd" "noatime" ];
      neededForBoot = true;

      encrypted = {
        enable = true;
        label = "PLAIN_DATA";
        blkDev = "/dev/disk/by-label/CRYPT_DATA";
      };
    };
  };

  # TODO: check if we are using pipewire
  hardware.pulseaudio.enable = mkForce false;

  services.avahi = {
    enable = true;
    publish.enable = true;
    nssmdns = true;
  };

}
