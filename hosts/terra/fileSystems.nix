{ lib, self, modulesPath, ... }:
with lib; {
  imports = [ (modulesPath + "/installer/scan/not-detected.nix") ];

  boot.initrd.luks.devices."crypt-disk0".device =
    "/dev/disk/by-uuid/ae9dbb6b-a107-444d-ac7f-2c687460b12a";
  boot.loader.efi.efiSysMountPoint = "/boot/efi";

  environment.state = {
    store = "/persist";

    directories = [ "/var/log" ];
    # files = [
    #   "/etc/NIXOS"
    # ];

  };

  fileSystems = {
    "/boot/efi" = {
      device = "/dev/disk/by-uuid/2E58-50CB";
      fsType = "vfat";
      neededForBoot = true;
    };

    "/" = mkDefault {
      device = "tmpfs";
      fsType = "tmpfs";

      options = [ "noatime" "nosuid" "size=1G" ];
    };

    "/tmp" = mkDefault {
      device = "tmpfs";
      fsType = "tmpfs";

      options = [ "noatime" "nodev" "nosuid" ];
    };

    "/persist" = {
      device = "/dev/disk/by-uuid/81638bab-559f-47a5-9178-80c8c9ef7511";
      fsType = "btrfs";
      options = [ "compress=zstd" "noatime" "subvol=persist" ];
      neededForBoot = true;
    };

    "/persist/nix" = {
      device = "/dev/disk/by-uuid/81638bab-559f-47a5-9178-80c8c9ef7511";
      fsType = "btrfs";
      options = [ "compress=zstd" "noatime" "subvol=nix" ];
      neededForBoot = true;
    };

    "/persist/home" = {
      device = "/dev/disk/by-uuid/81638bab-559f-47a5-9178-80c8c9ef7511";
      fsType = "btrfs";
      options = [ "compress=zstd" "noatime" "subvol=home" ];
      neededForBoot = true;
    };

    "/persist/var/log" = {
      device = "/dev/disk/by-uuid/81638bab-559f-47a5-9178-80c8c9ef7511";
      fsType = "btrfs";
      options = [ "compress=zstd" "noatime" "subvol=log" ];
      neededForBoot = true;
    };
  };
}
