{ lib, self, host, nixos-install-tools, writeShellApplication }:
with lib;
writeShellApplication {
  name = "nixos-install";

  runtimeInputs = [ nixos-install-tools ];

  text = ''
    DRY_RUN=''${DRY_RUN:-}
    dry_run() (
      if [[ -z "''${DRY_RUN}" ]]; then
        "$@"
        return $?
      else
        printf "%s\n" "$*" >&2
        return 0
      fi
    )

    install-system() (
      dry_run nixos-install --flake "path:${self}#${host}" "$@"
    )

    install-system "$@"
  '';
}
