{ lib, fileSystems, cryptsetup, jq, writeShellApplication }:
with lib;
let
  fileSystemNames =
    builtins.filter (fs: fileSystems.${fs}.options != [ "bind" ])
    (builtins.attrNames fileSystems);
  fileSystemsMountOrder = (toposort hasPrefix fileSystemNames).result;
  mountScripts = mapAttrs (_: fs: ''
    printf "%s" ${
      escapeShellArg (builtins.toJSON fs)
    } | mount_file_system "$root_path" "$@" || exit $?
  '') fileSystems;
in writeShellApplication {
  name = "setup-mnt";

  runtimeInputs = [ jq cryptsetup ];

  text = (builtins.readFile ./mount-file-system.sh) + ''
    root_path="''${1:-/mnt/}"
    dry_run umount --recursive "$root_path" || true
  '' + (concatStrings (map (fs: mountScripts.${fs}) fileSystemsMountOrder));
}
