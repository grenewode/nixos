#!/usr/bin/env bash

DRY_RUN="${DRY_RUN:-}"

dry_run() (
	if [[ -z "${DRY_RUN}" ]]; then
		"$@"
		return $?
	else
		printf "%s\n" "$*" >&2
		return 0
	fi
)

mount_file_system() (
	local root_path="${1:-/mnt/}"

	local config
	config="$(cat -)"

	local mount_point
	mount_point="${root_path}/$(jq -r ".mountPoint" <<<"${config}")"
	mount_point="$(realpath -m "${mount_point}" || exit $?)"

	local options
	options="$(jq -r ".options | join(\",\")" <<<"${config}")"

	local fs_type
	fs_type="$(jq -r ".fsType" <<<"${config}")"

	local device
	device="$(jq -r ".device" <<<"${config}")"

	local encrypted
	encrypted="$(jq -r ".encrypted.enable" <<<"${config}")"

	if [[ "${encrypted}" = "true" ]]; then

		local encrypted_blk_dev
		encrypted_blk_dev="$(jq -r ".encrypted.blkDev" <<<"${config}")"

		local encrypted_label
		encrypted_label="$(jq -r ".encrypted.label" <<<"${config}")"

		local encrypted_key_file
		encrypted_key_file="$(jq -r '.encrypted.keyFile // ""' <<<"${config}")"

		if [[ -n "${encrypted_key_file}" ]]; then
			printf "cannot decrypt %s: configurations with keyfiles are not supported, since the keyFile path is particular to the first state of booting\n" "${encrypted_blk_dev}" >&2
			exit 1
		fi

		local decrypt_counter=0

		dry_run cryptsetup close \
			"${encrypted_label}" >&2 || true

		while true; do
			dry_run cryptsetup open \
				--type luks \
				"${encrypted_blk_dev}" "${encrypted_label}" </dev/tty >&2

			local decrypt_ret=$?

			if [[ "$decrypt_ret" = "0" ]]; then
				break
			fi

			decrypt_counter=$(("$decrypt_counter" + 1))

			if [[ "$decrypt_counter" -ge 3 ]]; then
				return $decrypt_ret
			fi

			printf "unable to decrypt %s, trying again\n" "${encrypted_blk_dev}"
			sleep 1
		done

	fi

	dry_run mkdir -p "${mount_point}" || exit $?
	dry_run mount -o "${options}" -t "${fs_type}" "${device}" "${mount_point}" || exit $?
)
