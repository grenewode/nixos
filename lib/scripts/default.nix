{ self, config, lib, pkgs, host, ... }:

{

  system.build.scripts.setup-mnt =
    pkgs.callPackage ./setup-mnt { inherit (config) fileSystems; };

  system.build.scripts."nixos-install-${config.networking.hostName}" =
    pkgs.callPackage ./nixos-install { inherit host self; };

}
