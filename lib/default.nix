{ nixpkgs, ... }:
nixpkgs.lib.recursiveUpdate nixpkgs.lib ({
  recursiveUpdateAll = nixpkgs.lib.foldr nixpkgs.lib.recursiveUpdate { };
})

