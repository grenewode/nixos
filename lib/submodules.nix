{ root, exclude ? null, include ? null, recursive ? false }:
let
  items = builtins.readDir root;
  normalize = item: "${root}/${item}";

  itemsByKind = kind:
    map normalize
    (builtins.filter (name: items.${name} == kind) (builtins.attrNames items));

  files = itemsByKind "regular";
  directories = itemsByKind "directory";

in builtins.filter (item:
  ((include == null || (builtins.match include item) != null)
    && (exclude == null || (builtins.match exclude item) == null))) files
