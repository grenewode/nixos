{ pkgs, lib, config, modulesPath, self, ... }:
with lib;
let
  users = config.users.users;
  usernames = (builtins.attrNames users);

  normalUsernames =
    (builtins.filter (name: users.${name}.isNormalUser) usernames);

  homeManagerUsernames = if (config ? home-manager) then
    (builtins.filter (user: users.${user} ? home) normalUsernames)
  else
    [ ];

  autologinUser = if (builtins.length normalUsernames) == 1 then
    builtins.head normalUsernames
  else
    "root";
in {
  imports = [ "${modulesPath}/virtualisation/qemu-vm.nix" ];

  # By default qemu-vm sets this to [ "modesetting" ],
  # which apparently modeprobe can't find when the 
  # derivation is being built which kills everything.
  # So, rather than figure out the real reason, I've 
  # switched it off completely, and I guess we just 
  # have to do with out any kind of GUI.
  services.xserver.videoDrivers = mkOverride (-1) [ ];
  virtualisation.graphics = false;

  # this gets disabled when we are using the <nixpkgs/nixos/modules/profiles/headless.nix>
  # profile but since the VM is not headless, we want to make sure that we can actually log in
  systemd.services."serial-getty@ttyS0".enable = true;
  services.getty = { autologinUser = mkDefault "root"; };

  # We don't want to touch our actual files. Eventually,
  # we'll probably want to use a unionfs or something
  # so that we can see the actual files that should be
  # stored here... but not yet
  system.activationScripts = let
    store = strings.escapeShellArg config.environment.state.store;

    statefulUsers =
      (builtins.filter (user: users.${username}.home ? persistence)
        homeManagerUsernames);

    createStatefulUser = user:
      let
        username = user.home.username;
        store = strings.escapeShellArg user.home.state.store;
        group = config.users.users.${username}.group;
      in {
        name = "${username}-create-persist";
        value = ''
          if [[ ! -d ${store} ]];
          then
            echo "creating ${user.home.state.store} for user ${username} since we are in a VM testing environment"
            mkdir -p ${store}
            chown ${username}:${group} ${store}
          fi
        '';
      };

  in mkIf (config.environment ? state) ({
    create-persist = ''
      if [[ ! -d ${store} ]];
      then
        echo "creating ${config.environment.state.store} since we are in a VM testing environment"
        mkdir -p ${store}
      fi
    '';
  } // (builtins.listToAttrs (map createStatefulUser statefulUsers)));

  networking = {
    usePredictableInterfaceNames = false;
    interfaces = mkForce { eth0 = { useDHCP = true; }; };

    # This is needed for server installs, where the default gateway may be
    # configured for the hosting provider
    defaultGateway = mkForce null;
    defaultGateway6 = mkForce null;
  };
}
