name: module:
{ lib, config, ... }@args:
let cfg = config.presets.presets.${name};
in {
  options.presets.presets.${name} = {
    enable = lib.mkEnableOption "Enable preset ${name}";

    config = lib.mkOption {
      description = "configuration options for the ${name} preset";
      default = { };
      type = lib.types.submodule (import module);
    };
  };

  # imports = (lib.optional cfg.enable cfg.config.nixos);

  config = { system.nixos.tags = lib.mkIf cfg.enable [ "with-${name}" ]; };
}
