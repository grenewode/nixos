{ lib, pkgs, ... }: {
  i3 = rec {
    alt = "Mod1";
    cmd = "Mod4";
    hyper = "${cmd}+Shift";
    meh = "${cmd}";

    extraConfig = ''
      set $meh ${meh}
      set $hyper ${hyper}
    '';

    configFile = pkgs.writeText "wellknown.conf" extraConfig;
  };

  colors = {
    foreground = "#222222";
    background = "#eeeeee";
  };
}
