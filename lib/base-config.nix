{ pkgs, lib, self, config, host, nixpkgs, ... }:
with lib;
let
  nixPath = [ "nixpkgs=${nixpkgs}" ];
  nix-settings = rec {

    trusted-users = mkIf config.security.sudo.enable [ "root" "@wheel" ];

    substituters = [
      "https://prismlauncher.cachix.org"
      "https://nix-community.cachix.org"
      "https://cache.nixos.org"
    ];

    trusted-public-keys = [
      "prismlauncher.cachix.org-1:GhJfjdP1RFKtFSH3gXTIQCvZwsb2cioisOf91y/bK0w="
      "cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY="
      "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
    ];

    trusted-substituters = substituters;
  };
in {
  imports = [ "${self}/profiles/flakes.nix" ./scripts ];

  assertions = let
    inherit (config.system) stateVersion;
    inherit (lib.versions) majorMinor;
    pkgsVersion = majorMinor version;
  in [{
    assertion = versionAtLeast pkgsVersion stateVersion;
    message = ''
      system is stateVersion "${stateVersion} but
          nixpkgs is version to ${pkgsVersion}"'';
  }];

  virtualisation.vmVariant = { imports = [ ./vm.nix ]; };

  nix = if (versionAtLeast config.system.stateVersion "22.11" ) then {
    inherit nixPath;
    settings = nix-settings;
  } else {
    inherit nixPath;
    binaryCaches = nix-settings.substituters;
    trustedBinaryCaches = nix-settings.trusted-substituters;
    binaryCachePublicKeys = nix-settings.trusted-public-keys;
    trustedUsers = nix-settings.trusted-users;
  };

  nixpkgs.overlays = [ self.overlay ];

  time.timeZone = lib.mkDefault "America/Detroit";

  system.stateVersion = "23.05";
}
