{ lib, config, options, self, ... }:
with lib;
let cfg = config.environment.state;
in {
  options.environment.state = {
    store = mkOption {
      description = "the root path under which all state will be stored";
      type = types.str;

      default = "/nix/state";
    };

    files = mkOption {
      description = "the list of files which are stateful between reboots";
      type = with types; listOf anything;
      default = [
        "/etc/machine-id"
        {
          file = "/etc/nix/id_rsa";
          parentDirectory = { mode = "u=rwx,g=,o="; };
        }
      ];
    };

    directories = mkOption {
      description =
        "the list of directories which are stateful between reboots";
      type = with types; listOf str;
      default = [ ];
    };
  };

  config.environment.persistence."${cfg.store}" =
    mkIf (cfg.files != [ ] || cfg.directories != [ ]) {
      inherit (cfg) files directories;
    };

  config.environment.state.files = [ "/etc/machine-id" ]
    ++ (optionals config.services.openssh.enable
      (map ({ path, ... }: path) config.services.openssh.hostKeys));
  config.environment.state.directories =
    (optional config.systemd.coredump.enable "/var/lib/systemd/coredump");
}
