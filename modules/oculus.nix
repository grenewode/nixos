{ config, lib, pkgs, ... }:
with lib; {
  options.hardware.vr = {
    sidequest.enable =
      mkEnableOption "Enable support for the sidequest utility";
    sidequest.package = mkOption {
      description = "the package to install sidequest from";
      type = types.package;
      default = pkgs.sidequest;
    };
  };

  config.environment.systemPackages =
    (lists.optional config.hardware.vr.sidequest.enable
      config.hardware.vr.sidequest.package);
}
