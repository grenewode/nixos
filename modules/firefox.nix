{ config, pkgs, lib, self, ... }:
with lib;
let needsFirefoxModule = versionOlder "22.11" config.system.stateVersion;
in {
  options = mkIf needsFirefoxModule {
    programs.firefox = {
      enable = mkEnableOption "Enable the firefox program";
      package = mkOption {
        description = "the firefox package to use";
        type = types.package;
        default = self.packages.${pkgs.system}.firefox;
      };
    };
  };

  config = mkIf needsFirefoxModule {
    environment.systemPackages = lists.optional (config.programs.firefox.enable)
      config.programs.firefox.package;
  };

}
