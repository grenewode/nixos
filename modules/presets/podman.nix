{ config, pkgs, lib, ... }:
let
  inherit (lib) mkIf mkDefault optionals;
  podman = config.virtualisation.podman;
  containers = config.virtualisation.containers;
in {
  config = mkIf podman.enable {
    virtualisation.podman = {
      dockerCompat = mkDefault true;
      dockerSocket = { enable = mkDefault true; };
      defaultNetwork = { settings.dns_enabled = mkDefault true; };
    };

    virtualisation.containers = { enable = mkDefault true; };

    environment.state.directories = (optionals containers.enable
      [ containers.storage.settings.storage.graphroot ]);
  };
}
