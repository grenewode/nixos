{ config, lib, pkgs, ... }:
let
  inherit (config.programs) hyprland;
  inherit (lib) mkIf mkDefault;
in {
  programs.hyprland = mkIf hyprland.enable {
    xwayland = {
      enable = mkDefault true;
      hidpi = mkDefault true;
    };
  };
}
