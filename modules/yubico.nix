{ config, pkgs, lib, ... }:
with lib; {
  options.hardware.yubico = {
    enable = mkEnableOption "enable support for yubico devices";
  };

  config = mkIf config.hardware.yubico.enable {
    environment.systemPackages = with pkgs; [
      yubikey-personalization
      yubikey-personalization-gui
      yubikey-manager
      yubikey-manager-qt
      yubioath-flutter
    ];

    programs.ssh.startAgent = false;

    services.udev.packages = [ pkgs.libu2f-host pkgs.yubikey-personalization ];
    services.pcscd.enable = true;
  };
}
