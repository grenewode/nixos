{ pkgs, config, lib, ... }:
with lib; {
  options.hardware.spacemouse = {
    enable = mkEnableOption "enable support for 3dconnexion devices";

    spacenavd = mkOption {
      description = "the package which provides spnavd";

      type = types.package;
      default = pkgs.spacenavd;
    };
  };

  config = mkIf (config.hardware.spacemouse.enable) {
    assertions = [{
      assertion = !config.hardware.spacenavd.enable;
      message =
        "hardware.spacemouse.enable conflicts with hardware.spacenavd.enable";
    }];

    environment.systemPackages = with pkgs; [ spacenav-cube-example spnavcfg ];

    systemd.user.services.spacenavd = {
      description = "Daemon for the Spacenavigator 6DOF mice by 3Dconnexion";
      after = [ "syslog.target" ];
      wantedBy = [ "graphical.target" ];
      serviceConfig = {
        ExecStart = "${pkgs.spacenavd}/bin/spacenavd -d -l syslog";
      };
    };
  };
}
